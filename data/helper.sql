-- --------------------------------------------------------
-- 主机:                           39.105.114.220
-- 服务器版本:                        5.7.25 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 helper 的数据库结构
CREATE DATABASE IF NOT EXISTS `helper` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `helper`;

-- 导出  表 helper.activities 结构
CREATE TABLE IF NOT EXISTS `activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `describe` varchar(300) NOT NULL COMMENT '描述',
  `content` text NOT NULL COMMENT '内容',
  `state` tinyint(4) NOT NULL COMMENT '状态(0删除 1正常 2过期)',
  `sys_user_id` bigint(20) NOT NULL COMMENT '发布者',
  `sys_user_name` varchar(50) NOT NULL COMMENT '发布者昵称',
  `total` int(11) NOT NULL COMMENT '可参加人数',
  `total_now` int(11) DEFAULT '0' COMMENT '已参加人数',
  `is_host` tinyint(4) NOT NULL DEFAULT '0' COMMENT '热度是否靠前（0不是  1是）',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='活动';

-- 正在导出表  helper.activities 的数据：~23 rows (大约)
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` (`id`, `title`, `describe`, `content`, `state`, `sys_user_id`, `sys_user_name`, `total`, `total_now`, `is_host`, `create_date`, `update_date`) VALUES
	(9, '校运会', '本周五举行校运会，大家踊跃参加', '学校为丰富同学们的课余生活，特在本周五举行校运会，希望大家积极参加，踊跃报名', 2, 1, '管理员', 400, 80, 1, '2019-06-18 08:26:53', '2019-06-21 09:27:50'),
	(10, '大学生篮球超级联赛', '为了丰富在校大学生的课余生活，学校将于本周六开展大学生篮球超级联赛', '参加条件大二大三在校生，男女不限', 1, 1, '管理员', 230, 43, 0, '2019-06-18 09:41:17', '2019-06-21 08:34:17'),
	(11, '大学生篮球超级联赛', '为了丰富在校大学生的课余生活，学校将于本周六开展大学生篮球超级联赛', '参加条件大二大三在校生，男女不限', 2, 1, '管理员', 23, 43, 1, '2019-06-18 09:41:18', '2019-06-21 08:27:47'),
	(12, '歌咏比赛', '本周周一晚上7点在真知讲堂开阵歌咏比赛', '为丰富同学们的课余生活，现学校特在本周周一晚上7点在真知讲堂开阵歌咏比赛，希望同学们踊跃报名参加', 1, 1, '管理员', 47, 34, 0, '2019-06-18 13:59:50', '2019-06-21 08:34:11'),
	(13, '辩论赛', '3月2日学校将在公教楼313举办辩论赛，请同学们按时参加，有序进场', '3月2日学校将在公教楼313，就“科技文化艺术节是否应该收费的问题”举办辩论赛，请同学们按时参加，有序进场', 1, 1, '管理员', 44, 33, 0, '2019-06-18 14:03:23', '2019-06-21 08:34:08'),
	(20, '辩论赛', '本周五学校举行辩论赛', '为丰富学生们的课余生活，现学校将于本周五晚7点举行关于樱花节是否收费问题的辩论赛', 1, 1, '管理员', 44, 33, 1, '2019-06-21 08:27:26', '2019-06-21 08:27:26'),
	(21, '团中央的挑战杯', '本周五举行关于团日活动的团中央的挑战杯', '请大家准时参加', 1, 1, '管理员', 22, 1, 0, '2019-06-21 08:35:53', '2019-06-21 08:35:53'),
	(22, '学校的校庆', '下周四为学校成立28周年，届时将举行学校的校庆', '请大家准时参加', 2, 1, '管理员', 23, 23, 0, '2019-06-21 08:36:52', '2019-06-21 08:36:52'),
	(23, '大型唱歌比赛', '周五晚举行大型唱歌比赛', '大型唱歌比赛在真知讲堂举行，大家按时参加', 1, 1, '管理员', 23, 23, 0, '2019-06-21 08:37:38', '2019-06-21 08:37:38'),
	(24, '寝室美化大赛', '寝室美化大赛本周五举行', '大家准时参加', 1, 1, '管理员', 22, 11, 1, '2019-06-21 08:38:26', '2019-06-21 08:38:26'),
	(25, '超级棋王争霸赛', '超级棋王争霸赛周六举行', '大家准时参加', 2, 1, '管理员', 44, 22, 0, '2019-06-21 08:38:54', '2019-06-21 08:38:54'),
	(26, '校园涂鸦大赛', '校园涂鸦大赛周天举行', '大家准时参加', 1, 1, '管理员', 43, 23, 0, '2019-06-21 08:45:45', '2019-06-21 08:45:45'),
	(27, '水果蔬菜艺术创意大赛', '水果蔬菜艺术创意大赛', '水果蔬菜艺术创意大赛', 2, 1, '管理员', 23, 23, 1, '2019-06-21 08:46:12', '2019-06-21 08:46:12'),
	(28, '校园达人大赛', '校园达人大赛', '校园达人大赛', 1, 1, '管理员', 34, 23, 0, '2019-06-21 08:46:32', '2019-06-21 08:46:32'),
	(29, '春游', '春游', '春游', 2, 1, '管理员', 45, 34, 0, '2019-06-21 08:46:46', '2019-06-21 08:46:46'),
	(30, '荧光夜跑活动', '荧光夜跑活动', '荧光夜跑活动', 1, 1, '管理员', 34, 23, 1, '2019-06-21 08:47:28', '2019-06-21 08:47:28'),
	(31, '定向越野', '定向越野', '定向越野', 2, 1, '管理员', 54, 34, 1, '2019-06-21 08:47:47', '2019-06-21 08:47:47'),
	(32, '读书节活动', '读书节活动', '读书节活动', 1, 1, '管理员', 45, 34, 0, '2019-06-21 08:48:01', '2019-06-21 08:48:01'),
	(33, '视频配音大赛', '视频配音大赛', '视频配音大赛', 2, 1, '管理员', 34, 23, 1, '2019-06-21 08:48:24', '2019-06-21 08:48:24'),
	(34, '校园吉尼斯', '校园吉尼斯', '校园吉尼斯', 1, 1, '管理员', 45, 34, 0, '2019-06-21 08:48:46', '2019-06-21 08:48:46'),
	(35, '手绘环保袋活动', '手绘环保袋活动', '手绘环保袋活动', 2, 1, '管理员', 45, 34, 0, '2019-06-21 08:49:02', '2019-06-21 08:49:02'),
	(36, '知识王大赛', '知识王大赛', '知识王大赛', 1, 1, '管理员', 45, 34, 1, '2019-06-21 08:49:19', '2019-06-21 08:49:19'),
	(37, '募集图书', '给贫困山区捐书', '给贫困山区捐书', 1, 20, '职业技术学院青协', 30, 0, 0, '2019-06-21 09:33:54', '2019-06-21 09:33:54');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;

-- 导出  表 helper.goods 结构
CREATE TABLE IF NOT EXISTS `goods` (
  `good_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id主键',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '标题',
  `describe` varchar(50) DEFAULT '' COMMENT '描述',
  `price` int(11) DEFAULT NULL COMMENT '价格',
  `address` varchar(30) DEFAULT '' COMMENT '卖主地址',
  `content` varchar(300) DEFAULT '' COMMENT '图片',
  `state` int(1) NOT NULL DEFAULT '1' COMMENT '状态(0删除 1正常)',
  `sys_user_id` bigint(20) NOT NULL COMMENT '卖主id',
  `sys_user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '卖主昵称',
  `release_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间（排序用）（擦亮会更新发布时间）',
  `is_host` int(1) NOT NULL DEFAULT '0' COMMENT '热度是否靠前（0不是  1是）',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`good_id`)
) ENGINE=InnoDB AUTO_INCREMENT=234523136 DEFAULT CHARSET=utf8;

-- 正在导出表  helper.goods 的数据：~23 rows (大约)
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` (`good_id`, `title`, `describe`, `price`, `address`, `content`, `state`, `sys_user_id`, `sys_user_name`, `release_date`, `is_host`, `create_date`, `update_date`) VALUES
	(12343223, '出售小说', '出售小说', 2, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:40:52', 0, '2019-06-21 08:40:52', '2019-06-21 08:41:29'),
	(23432323, '出售计科课本', '出售计科课本', 1, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:37:09', 0, '2019-06-21 08:37:09', '2019-06-21 08:41:32'),
	(45676543, '出售栗子', '有型又脆', 12, '师大', '', 1, 0, '', '2019-06-21 08:44:52', 0, '2019-06-21 08:44:52', '2019-06-21 08:44:52'),
	(123112312, '出售小说', '出售小说', 12, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:38:56', 0, '2019-06-21 08:38:56', '2019-06-21 08:41:27'),
	(123123123, '出售小说', '出售小说', 23, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:40:23', 0, '2019-06-21 08:40:23', '2019-06-21 08:41:26'),
	(123123132, '出售小说', '出售小说', 123, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:39:40', 0, '2019-06-21 08:39:40', '2019-06-21 08:41:25'),
	(123213123, '出售手机', '苹果手机', 9999, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:42:01', 0, '2019-06-21 08:42:01', '2019-06-21 08:42:29'),
	(123213232, '出售水果', '又好又甜', 123, '师大', '', 1, 0, '', '2019-06-21 08:45:17', 0, '2019-06-21 08:45:17', '2019-06-21 08:45:17'),
	(123432123, '收购二手键盘', '收购二手键盘', 1, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:38:01', 0, '2019-06-21 08:38:01', '2019-06-21 08:41:24'),
	(123456322, '出售计科课本', '出售计科课本', 1, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:36:58', 0, '2019-06-21 08:36:58', '2019-06-21 08:41:23'),
	(123456543, '一个标题', '一个标题', 123, '启智园10号楼', '', 0, 0, '', '2019-06-21 08:36:03', 1, '2019-06-21 08:36:03', '2019-06-21 09:22:59'),
	(123456796, '一个标题', '出售桌上床', 30, '师大', '9成新', 1, 123, '123', '2019-06-20 15:58:36', 0, '2019-06-20 15:58:36', '2019-06-20 15:58:36'),
	(123456797, '一个标题', '出售桌上床', 40, '师大dsf', '9成新', 1, 123, '123', '2019-06-20 15:58:36', 1, '2019-06-20 15:58:36', '2019-06-21 09:23:49'),
	(123456808, '一个标题', '出售桌上床', 30, '师大', '9成新', 0, 0, '123', '2019-06-20 18:20:13', 1, '2019-06-20 18:20:13', '2019-06-21 09:23:09'),
	(123456809, '出售二手机械键盘', '原价210元，用了一段时间9成新，现在不太需要了想出售。', 120, '启智园10号楼', '', 1, 9, '王英杰', '2019-06-20 18:25:37', 0, '2019-06-20 18:25:37', '2019-06-20 18:25:37'),
	(123456810, '收购二手键盘', '想要收购一个便宜的机械键盘', 90, '启智园10号楼', '', 1, 9, '王英杰', '2019-06-20 18:28:41', 0, '2019-06-20 18:28:41', '2019-06-21 08:41:04'),
	(123456819, '出售算法导论一本', '买了之后没有看过，9.9成新，半价出售', 30, '启智园10号楼', '', 1, 9, '王英杰', '2019-06-21 08:17:16', 0, '2019-06-21 08:17:16', '2019-06-21 08:17:16'),
	(123456820, '出售计科课本', '给钱就卖', 1, '启智园10号楼', '', 1, 9, '王英杰', '2019-06-21 08:36:25', 0, '2019-06-21 08:36:25', '2019-06-21 08:36:25'),
	(234523111, '出售计科课本', '出售计科课本', 1, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:36:29', 0, '2019-06-21 08:36:29', '2019-06-21 08:41:05'),
	(234523112, '出售计科课本', '出售计科课本', 21, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:37:17', 0, '2019-06-21 08:37:17', '2019-06-21 08:41:08'),
	(234523113, '出售电脑', '电脑', 123, '启智园10号楼', '', 1, 0, '', '2019-06-21 08:40:54', 0, '2019-06-21 08:40:54', '2019-06-21 08:41:47'),
	(234523134, '出售苹果', '有大又圆', 12, '师大', '', 1, 0, '', '2019-06-21 08:44:23', 0, '2019-06-21 08:44:23', '2019-06-21 08:44:23'),
	(234523135, '出售鼠标', '出售二手鼠标', 10, '师大', '', 1, 9, '王英杰', '2019-06-21 09:37:51', 0, '2019-06-21 09:37:51', '2019-06-21 09:37:51');
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;

-- 导出  表 helper.job 结构
CREATE TABLE IF NOT EXISTS `job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(50) NOT NULL COMMENT '招聘标题',
  `describe` varchar(300) DEFAULT NULL COMMENT '描述',
  `wage` bigint(6) DEFAULT NULL COMMENT '工资',
  `address` varchar(30) DEFAULT NULL COMMENT '地址',
  `content` varchar(100) DEFAULT NULL COMMENT '内容',
  `state` tinyint(4) NOT NULL COMMENT '状态（0删除 1正常 2过期）',
  `sys_user_id` bigint(20) NOT NULL COMMENT '发布者id',
  `sys_user_name` varchar(20) DEFAULT NULL COMMENT '发布者昵称',
  `total` int(10) DEFAULT NULL COMMENT '可参加人数',
  `total_now` int(10) DEFAULT NULL COMMENT '已参加人数',
  `activities_time` date NOT NULL COMMENT '活动时间',
  `is_host` tinyint(4) NOT NULL COMMENT '热度是否靠前（0不是 1是）',
  `end_time` date NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100008 DEFAULT CHARSET=utf8;

-- 正在导出表  helper.job 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` (`job_id`, `title`, `describe`, `wage`, `address`, `content`, `state`, `sys_user_id`, `sys_user_name`, `total`, `total_now`, `activities_time`, `is_host`, `end_time`) VALUES
	(100005, '打老寇', '把老寇打一顿', 1, '河北师范大学', '筒子们，打老寇啊', 1, 17, 'xhh', 1000, 0, '2019-06-18', 1, '2019-07-30'),
	(100006, '后台测试', '测试程序bug', 1, '河北师范大学', '测试程序bug', 1, 17, 'xhh', 10, 0, '2019-06-19', 1, '2019-06-26');
/*!40000 ALTER TABLE `job` ENABLE KEYS */;

-- 导出  表 helper.lost_find 结构
CREATE TABLE IF NOT EXISTS `lost_find` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `pic` varchar(1000) NOT NULL COMMENT '封面图片',
  `describe` varchar(50) NOT NULL COMMENT '描述',
  `content` varchar(100) NOT NULL COMMENT '内容',
  `con_pic` varchar(50) NOT NULL COMMENT '详细图片',
  `state` bigint(20) NOT NULL DEFAULT '0' COMMENT '状态(0过期	1正常	2已经找到  3已删除)',
  `audit` bigint(20) NOT NULL COMMENT '审核状态（0未审核 1审核通过 2审核未通过）',
  `type` bigint(20) NOT NULL COMMENT '类型(0失主发布寻找东西，1捡到者发布寻找失主)',
  `qq` varchar(50) NOT NULL COMMENT 'qq',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='失物招领';

-- 正在导出表  helper.lost_find 的数据：~21 rows (大约)
/*!40000 ALTER TABLE `lost_find` DISABLE KEYS */;
INSERT INTO `lost_find` (`id`, `title`, `pic`, `describe`, `content`, `con_pic`, `state`, `audit`, `type`, `qq`, `user_id`, `create_date`, `update_date`) VALUES
	(1, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '本人于一食堂丢失饭卡一张，请捡到的同学及时与我联系，感谢。', '143534243', 1, 1, 0, '2132178575kl', 2019001, '2019-06-18 17:07:08', '2019-06-18 17:07:08'),
	(2, '丢失钱包', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在四食堂丢失钱包一个1', '本人于2019年6月10日在四食堂的第五档口买完饭之后发现钱包丢失', '344123', 1, 1, 0, '54651231', 2019002, '2019-06-18 17:17:41', '2019-06-18 17:17:43'),
	(3, '丢失眼镜', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在公教楼丢失眼镜', '本人在公教楼A306上完课后忘记拿眼镜，回去没找到，请捡到的同学跟我联系', '3445', 1, 1, 0, '875757857', 2019003, '2019-06-18 17:25:35', '2019-06-18 17:25:35'),
	(4, '捡到钥匙', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在图书挂捡到钥匙', '本人在图书馆三楼共享空间捡到钥匙一串，钥匙上带有hellokitty的钥匙链，请失主尽快与我联系', '3233', 1, 1, 1, '34323423', 2019011, '2019-06-18 18:45:36', '2019-06-18 18:45:58'),
	(6, '捡到U盘', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在打印店捡到U盘', '本人在师活打印店捡到128G金士顿U盘一个，里面与计算机专业的复习材料，请失主及时与我联系。', '114245254', 1, 1, 1, '324320423', 20190113, '2019-06-18 18:58:42', '2019-06-18 18:58:53'),
	(8, '丢失U盘', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '师活超市', '在师活丢失u盘', '123432', 0, 0, 0, '1111111111', 0, '2019-06-21 08:46:40', '2019-06-21 08:46:40'),
	(9, '丢失眼镜', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在公教楼丢失眼镜', '', '', 0, 0, 0, '', 0, '2019-06-21 08:46:47', '2019-06-21 08:46:47'),
	(10, '丢失眼镜', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在公教楼丢失眼镜', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:03', '2019-06-21 08:47:03'),
	(11, '丢失眼镜', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在公教楼丢失眼镜', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:07', '2019-06-21 08:47:07'),
	(12, '丢失眼镜', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在公教楼丢失眼镜', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:09', '2019-06-21 08:47:09'),
	(13, '丢失眼镜', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在公教楼丢失眼镜', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:15', '2019-06-21 08:47:15'),
	(14, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:17', '2019-06-21 08:47:17'),
	(15, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:19', '2019-06-21 08:47:19'),
	(16, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:23', '2019-06-21 08:47:23'),
	(17, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:29', '2019-06-21 08:47:29'),
	(18, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:32', '2019-06-21 08:47:32'),
	(19, '丢失饭卡', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在一食堂丢失饭卡', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:35', '2019-06-21 08:47:35'),
	(20, '捡到钥匙', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在图书挂捡到钥匙', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:39', '2019-06-21 08:47:39'),
	(21, '捡到钥匙', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在图书挂捡到钥匙', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:42', '2019-06-21 08:47:42'),
	(22, '捡到钥匙', ' http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在图书挂捡到钥匙', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:45', '2019-06-21 08:47:45'),
	(23, '捡到钥匙', 'http://help-1255334830.file.myqcloud.com/20190619/875d484747454f959cc6a195e2107a43.png', '在图书挂捡到钥匙', '', '', 0, 0, 0, '', 0, '2019-06-21 08:47:49', '2019-06-21 08:47:49');
/*!40000 ALTER TABLE `lost_find` ENABLE KEYS */;

-- 导出  表 helper.sys_menu 结构
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `parent_id` bigint(20) NOT NULL COMMENT '上级菜单id（0为顶级）',
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `url` varchar(50) DEFAULT NULL COMMENT '菜单url',
  `icon` varchar(50) NOT NULL COMMENT '图标',
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='菜单';

-- 正在导出表  helper.sys_menu 的数据：~13 rows (大约)
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `icon`) VALUES
	(1, 0, '系统管理', NULL, 'layui-icon-auz'),
	(2, 1, '菜单管理', '/modules/sys/sysMenu.html', 'layui-icon-menu-fill'),
	(5, 1, '用户管理', '/modules/sys/sysUser.html', 'layui-icon-user'),
	(36, 36, '角色管理', '', 'layui-icon-face-smile-b'),
	(37, 1, '角色管理', '/modules/sys/sysRole.html', 'layui-icon-face-smile-b'),
	(40, 0, '跳蚤市场', NULL, 'layui-icon-menu-fill'),
	(42, 40, '商品管理', '/modules/goods/goods.html', 'layui-icon-rmb'),
	(43, 0, '失物招领', NULL, 'layui-icon-rate'),
	(44, 43, '信息管理', '/modules/lost/lost.html', 'layui-icon-star-fill'),
	(45, 0, '校内活动', NULL, 'layui-icon-fire'),
	(46, 45, '活动管理', '/modules/activity/activities.html', 'layui-icon-theme'),
	(48, 0, '兼职平台', NULL, 'layui-icon-rmb'),
	(49, 48, '兼职发布', '/modules/job/job.html', 'layui-icon-table');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;

-- 导出  表 helper.sys_role 结构
CREATE TABLE IF NOT EXISTS `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_name` varchar(20) NOT NULL COMMENT '角色名称',
  `note` varchar(20) NOT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='角色';

-- 正在导出表  helper.sys_role 的数据：~6 rows (大约)
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` (`role_id`, `role_name`, `note`) VALUES
	(1, '管理员', '管理员'),
	(15, '社团', '管理活动'),
	(16, '兼职管理员', '兼职管理员'),
	(17, '失物招领管理员', '失物招领'),
	(18, '跳蚤市场管理员', '二手交易'),
	(19, 'test', 'TEST');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;

-- 导出  表 helper.sys_role_menu 结构
CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `role_menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`role_menu_id`),
  UNIQUE KEY `role_id_menu_id` (`role_id`,`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COMMENT='角色对应菜单';

-- 正在导出表  helper.sys_role_menu 的数据：~14 rows (大约)
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` (`role_menu_id`, `role_id`, `menu_id`) VALUES
	(99, 15, 45),
	(100, 15, 46),
	(93, 16, 48),
	(94, 16, 49),
	(95, 17, 43),
	(96, 17, 44),
	(97, 18, 40),
	(98, 18, 42),
	(105, 19, 40),
	(106, 19, 42),
	(107, 19, 45),
	(108, 19, 46),
	(109, 19, 48),
	(110, 19, 49);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;

-- 导出  表 helper.sys_user 结构
CREATE TABLE IF NOT EXISTS `sys_user` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(10) NOT NULL COMMENT '账号',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `nick_name` varchar(50) NOT NULL COMMENT '昵称',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态（0非正常   1 正常   2删除）',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) NOT NULL COMMENT '手机号',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='后台用户';

-- 正在导出表  helper.sys_user 的数据：~8 rows (大约)
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` (`user_id`, `username`, `password`, `nick_name`, `state`, `email`, `phone`, `create_date`, `update_date`) VALUES
	(1, 'admin', 'YWRtaW4x', '管理员', 1, 'ykaifei@163.com', '13730404063', '2019-05-23 22:08:13', '2019-06-21 16:56:52'),
	(5, 'qwe1', 'qwe', 'qwe', 1, 'qwe', '12354668798', '2019-05-25 14:37:19', '2019-05-25 14:54:27'),
	(8, '123', 'MTIz', '123123', 1, '123123', '12345678900', '2019-06-01 15:12:41', '2019-06-01 15:25:55'),
	(17, 'xhh', 'eGho', '小灰灰', 1, 'r@r.r', '15603103977', '2019-06-09 10:56:42', '2019-06-09 10:56:42'),
	(19, 'ceshi', 'Y2VzaGk=', 'cesi', 1, 'ceshi@qq.com', '12345678900', '2019-06-18 11:54:51', '2019-06-18 11:54:51'),
	(20, 'zyjsxyqx', 'MTIz', '职业技术学院青协', 1, '34343@qq.com', '15830483432', '2019-06-18 15:36:30', '2019-06-18 15:36:30'),
	(21, '111', 'MTEx', '111', 1, '23213122312@qq.com', '12323442334', '2019-06-20 17:32:30', '2019-06-20 17:32:30'),
	(24, 'lz', 'bHo=', 'llz', 1, 'gengt@yeah.net', '12222222222', '2019-06-21 09:21:31', '2019-06-21 09:21:31');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;

-- 导出  表 helper.sys_user_role 结构
CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `user_id_role_id` (`user_id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='角色与用户对应关系';

-- 正在导出表  helper.sys_user_role 的数据：~7 rows (大约)
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` (`user_role_id`, `user_id`, `role_id`) VALUES
	(18, 1, 1),
	(5, 8, 10),
	(7, 17, 1),
	(10, 19, 13),
	(13, 20, 15),
	(12, 21, 14),
	(17, 24, 19);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;

-- 导出  表 helper.user 结构
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `st_no` varchar(40) NOT NULL COMMENT '学号',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别(0女  1男)',
  `head_pic` varchar(200) DEFAULT NULL COMMENT '头像',
  `jg_id` varchar(100) DEFAULT NULL COMMENT '学院',
  `bh_id` varchar(100) DEFAULT NULL COMMENT '专业',
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `st_no` (`st_no`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='c端用户';

-- 正在导出表  helper.user 的数据：~9 rows (大约)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `st_no`, `name`, `sex`, `head_pic`, `jg_id`, `bh_id`, `create_date`, `update_date`) VALUES
	(5, '2016014923', '樊士博', '男', NULL, '信息技术学院', '16计算机科学与技术(对口)1班', '2019-06-19 22:14:48', '2019-06-19 22:14:48'),
	(8, '2016014926', '杨凯飞', '男', NULL, '信息技术学院', '16计算机科学与技术(对口)1班', '2019-06-19 22:44:27', '2019-06-19 22:44:27'),
	(9, '2016014933', '王英杰', '男', NULL, '信息技术学院', '16计算机科学与技术(对口)1班', '2019-06-20 06:46:41', '2019-06-20 06:46:41'),
	(10, '2016014951', '刘文璐', '女', NULL, '信息技术学院', '16计算机科学与技术(对口)2班', '2019-06-20 08:01:49', '2019-06-20 08:01:49'),
	(11, '2016014983', '刘明敏', '女', NULL, '信息技术学院', '16计算机科学与技术(对口)2班', '2019-06-20 08:46:03', '2019-06-20 08:46:03'),
	(12, '2016014942', '刘肇', '男', NULL, '信息技术学院', '16计算机科学与技术(对口)1班', '2019-06-20 09:16:54', '2019-06-20 09:16:54'),
	(13, '2016014916', '李健威', '男', NULL, '信息技术学院', '16计算机科学与技术(对口)1班', '2019-06-20 09:54:38', '2019-06-20 09:54:38'),
	(14, '2017016036', '郭山', '男', NULL, '信息技术学院', '17计算机科学与技术(对口)1班', '2019-06-20 12:04:23', '2019-06-20 12:04:23'),
	(16, '2017016051', '王天城', '男', NULL, '信息技术学院', '17计算机科学与技术(对口)2班', '2019-06-20 12:05:24', '2019-06-20 12:05:24');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- 导出  表 helper.user_activities 结构
CREATE TABLE IF NOT EXISTS `user_activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `activities_id` bigint(20) NOT NULL COMMENT '活动id',
  `phone` varchar(11) NOT NULL COMMENT '手机号',
  `qq` varchar(12) DEFAULT NULL COMMENT 'qq',
  `wx` varchar(20) DEFAULT NULL COMMENT '微信',
  `state` tinyint(4) NOT NULL COMMENT '0待审核   1已完成   2未守约  3等待 4不符合要求',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_activities_id` (`user_id`,`activities_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户参加活动表';

-- 正在导出表  helper.user_activities 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `user_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_activities` ENABLE KEYS */;

-- 导出  表 helper.user_goods 结构
CREATE TABLE IF NOT EXISTS `user_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id主键',
  `st_no` bigint(20) NOT NULL COMMENT '学生学号',
  `good_id` bigint(20) NOT NULL COMMENT '货物编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  helper.user_goods 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `user_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_goods` ENABLE KEYS */;

-- 导出  表 helper.user_job 结构
CREATE TABLE IF NOT EXISTS `user_job` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
  `st_no` bigint(20) NOT NULL COMMENT '学生学号',
  `job_id` bigint(20) NOT NULL COMMENT '兼职id',
  `resume_url` varchar(50) NOT NULL COMMENT '简历url',
  `state` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- 正在导出表  helper.user_job 的数据：~10 rows (大约)
/*!40000 ALTER TABLE `user_job` DISABLE KEYS */;
INSERT INTO `user_job` (`id`, `st_no`, `job_id`, `resume_url`, `state`) VALUES
	(1, 2016014940, 100001, '1', 1),
	(2, 2016014941, 100001, '1', 1),
	(3, 2016014942, 100001, '1', 1),
	(4, 2016014943, 100001, '1', 0),
	(5, 2016014944, 100001, '1', 0),
	(6, 2016014941, 100001, '1', 1),
	(7, 2016014942, 100001, '1', 1),
	(8, 2016014943, 100001, '1', 1),
	(9, 2016014944, 100001, '1', 1),
	(10, 23333333, 100005, '123', 1);
/*!40000 ALTER TABLE `user_job` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
