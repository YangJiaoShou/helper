项目结构


admin 运营后台


api 小程序接口


工程依赖说明


不允许直接依赖 dao, common

 admin 与 api 依赖 service
 service 依赖 dao, common

dao 模块

存放与数据库表一一对应的 dao,  entity, mapper 
核心使用 mybatis, mybatis-plus



service 模块

提供方法给 admin, api使用
禁止 admin, api直接使用 dao 查询



common模块

提供一些共有方法与工具等



使用到的技术框架



spring boot


http://spring.io



thymeleaf


https://thymeleaf.org 



mybatis-plus


http://mp.baomidou.com



druid


https://github.com/alibaba/druid


layui


https://www.layui.com/
