package com.hebtu.helper.common.constants;

/**
 * 爬虫常量信息
 * @author YKF
 * @date 2019/6/18 20:41
 */
public class LoginMessage {

    /**
     * 基础地址
     */
    public static final String BASE_URL="http://202.206.100.217";

    /**
     * 登陆URL
     */
    public static final String LOGIN_URL=BASE_URL+"/xtgl/login_slogin.html";

    /**
     * 课程表
     */
    public static final String KBCX = BASE_URL + "/kbcx/xskbcx_cxXsKb.html";

    /**
     * 个人信息URL
     */
    public static final String MYSELF=BASE_URL+"/xsxxxggl/xsxxwh_cxCkDgxsxx.html?gnmkdm=N100801";

    /**
     * 登陆密匙
     */
    public static final String LOGIN_PASS_URL=BASE_URL+"/xtgl/login_getPublicKey.html";

    /**
     * 账号或者密码不能为空
     */
    public static final String CHECK_ERROR ="用户名或密码不正确，请重新输入";

    /**
     *用户名不能为空！
     */
    public static final String USER_NULL_ERROR ="用户名不能为空";

    /**
     * 密码不能为空
     */
    public static final String PASSWORD_NULL_ERROR ="密码不能为空";

}
