package com.hebtu.helper.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 提取html网页内容工具
 * @author YKF
 * @date 2019/6/18 20:39
 */
public class HtmlTools {

    /**
     * 查找input的name值为csrftoken的value
     * @param html HTML文档
     * @return 返回
     */
    public static String findCsrftoken(String html) {
        String res="";
        String pattern="<input type=\"hidden\" id=\"csrftoken\" name=\"csrftoken\" value=\"(.*?)\"/>";

        Pattern p=Pattern.compile(pattern);
        Matcher m=p.matcher(html);

        if(m.find()){
            res=m.group();
            res=res.substring(res.indexOf("value=\"")+7,res.lastIndexOf("\""));
        }
        return res;
    }
}
