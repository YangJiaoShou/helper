package com.hebtu.helper.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author YKF
 * @date 2019/5/19 16:47
 */
public class H extends HashMap<String ,Object> {

    private static final long serialVersionUID = 1L;

    /**
     * 默认返回信息
     */
    public H(){
        put("code",0);
        put("msg", "success");
    }

    /**
     * 默认error，错误代码为500
     * @return
     */
    public static H error(){
        return error(500,"尼玛炸了");
    }

    /**
     * 自定义500错误信息
     * @param msg  错误文字信息
     * @return
     */
    public static H error(String msg){
        return error(500,msg);
    }

    /**
     * 错误信息
     * @param code  状态码
     * @param msg  文字信息
     * @return
     */
    public static H error(int code,String msg){
        H h=new H();
        h.put("code",code);
        h.put("msg",msg);
        return h;
    }

    /**
     * 重写成功返回信息
     * @param msg
     * @return
     */
    public static H success(String msg) {
        H h = new  H();
        h.put("msg", msg);
        return h;
    }
    /**
     * 封装返回数据
     * @param map
     * @return
     */
    public static H success(Map<String, Object> map) {
        H h=new H();
        h.putAll(map);
        return h;
    }

    /**
     * 默认信息
     * @return
     */
    public static H success() {
        return new H();
    }

    @Override
    public H put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
