package com.hebtu.helper.common.utils;

import java.io.Serializable;
import java.util.List;

/**
 * layui分页返回数据格式接口定义
 * @author YKF
 * @date 2019/5/19 16:56
 */
public class LayuiPage implements Serializable {
    private static final long serialVersionUID = 1L;
    private int code = 0;
    //总记录数
    private long count;
    //列表数据
    private List<?> data;

    public LayuiPage(List<?> data, long count) {
        this.data = data;
        this.count = count;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}