package com.hebtu.helper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author YKF
 * @date 2019/6/15 15:11
 */
@SpringBootApplication
public class ApiApplication {

    public static void main(String[] args) {
            SpringApplication.run(ApiApplication.class, args);
        }
}
