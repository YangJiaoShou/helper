package com.hebtu.helper.modules.job.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.job.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 兼职 查询兼职列表
 * @author Tremor
 * @date 2019年6月18日 16点00分
 */

@RequestMapping("/job")
@RestController
public class JobController {

    @Autowired
    JobService jobService;

    @RequestMapping("/list")
    public H list(@RequestParam Map<String,Object> params){
        return H.success().put("data", jobService.listJobAPI(params));
    }
}
