package com.hebtu.helper.modules.lost.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.lost.entity.LostFindEntity;
import com.hebtu.helper.modules.lost.service.LostFindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequestMapping("/find")
@RestController
public class FindController {

    @Autowired
    private LostFindService lostFindService;

    /**
     * 寻找失主，捡到的物品信息
     * @param params
     * @return
     */
    @RequestMapping("/list")
    public H list(@RequestParam Map<String, Object> params){
        params.put("type",1);
        return H.success().put("data", lostFindService.listLost(params));
    }

    /**
     * 捡到者发布详细的物品信息
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/insert")
    public H insert(LostFindEntity lostFindEntity){
        lostFindEntity.setType(1);
        return lostFindService.insertLostAPI(lostFindEntity)>0?H.success():H.error(203,"保存失败");
    }

    /**
     * 捡到物品的同学删除自己所发的消息
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/del")
    public H  del(LostFindEntity lostFindEntity){
        return lostFindService.delLostAPI(lostFindEntity)>0?H.success():H.error(203,"删除失败");
    }

    /**
     * 捡到物品的同学修改自己所发的消息
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/update")
    public H update(LostFindEntity lostFindEntity){
        return lostFindService.updateAPI(lostFindEntity)>0?H.success():H.error(203,"修改失败");
    }
}
