package com.hebtu.helper.modules.job.controller;


import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.job.entity.UserJobEntity;
import com.hebtu.helper.modules.job.service.UserJobService;
import com.hebtu.helper.modules.user.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 兼职 用户报名
 * @author Tremor
 * @date 2019年6月18日 16点00分
 */

@RequestMapping("/userJob")
@RestController
public class userJobController {

    @Autowired
    UserJobService userJobService;

    @RequestMapping("/delete")
    public H deleteByJobId(UserJobEntity userJobEntity){
        return userJobService.deleteUserJobAPI(userJobEntity)==1?H.success("删除成功"):H.error("不存在");
    }

    @RequestMapping("/update")
    public H updateUserJobById(UserJobEntity userJobEntity){
        userJobEntity.setState(1);
        return userJobService.updateUserJobAPI(userJobEntity)==1?H.success("修改成功"):H.error("修改失败");
    }


    @RequestMapping("/insert")
    public H insert(UserJobEntity userJobEntity){
        userJobEntity.setState(1);
        return userJobService.insertUserJobAPI(userJobEntity)?H.success("报名成功"):H.error("报名失败");
    }

    //待施工
    @RequestMapping("/query")
    public H query(Long stNo){
        return H.success().put("Data",userJobService.queryByStNoAPI(stNo));
//        return null;
    }
//    @RequestMapping("/query/{stNo}")
//    public H query(@PathVariable Long stNo){
//        return H.success().put("data",userJobService.queryByStNoAPI(stNo));
////        return null;
//    }
}
