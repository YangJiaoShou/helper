package com.hebtu.helper.modules.activitys.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.activities.entity.ActivitiesEntity;
import com.hebtu.helper.modules.activities.service.ActivitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 校内活动
 */
@RequestMapping("/activitys")
@RestController
public class ActivitysController {

    @Autowired
    private ActivitiesService activitiesService;

    /**
     * 用户查询所有正常状态的记录并进行显示，对activitys的信息查询查询
     * @return
     */
    @RequestMapping("/list")
    public H list(){
        Map<String ,Object> map=new HashMap<>();
        map.put("state",1);
        List<ActivitiesEntity> activitiesEntities= activitiesService.selectByMap(map);
        return H.success().put("data", activitiesEntities);
    }


}
