package com.hebtu.helper.modules.lost.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.lost.entity.LostFindEntity;
import com.hebtu.helper.modules.lost.service.LostFindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 失物招领 丢失
 * @author YKF
 * @date 2019/6/15 14:33
 */
@RequestMapping("/lost")
@RestController
public class LostController {

    @Autowired
    private LostFindService lostFindService;

    /**
     *用户丢失物品的详细描述
     * @param params
     * @return
     */
    @RequestMapping("/list")
    public H list(@RequestParam Map<String, Object> params){
        params.put("type", 0);
        return H.success().put("data", lostFindService.listLost(params));
    }

    /**
     * 用户发布所丢失物品的描述信息
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/insert")
    public H insert(LostFindEntity lostFindEntity){
        lostFindEntity.setType(0);
        return lostFindService.insertLostAPI(lostFindEntity)>0?H.success():H.error(203,"保存失败");
    }

    /**
     * 用户删除丢失物品信息
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/del")
    public H  del(LostFindEntity lostFindEntity){
        lostFindEntity.setType(0);
        return lostFindService.delLostAPI(lostFindEntity)>0?H.success():H.error(203,"删除失败");
    }

    /**
     * 用户修改丢失物品信息
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/update")
    public H update(LostFindEntity lostFindEntity){
        lostFindEntity.setType(0);
        return lostFindService.updateAPI(lostFindEntity)>0?H.success():H.error(203,"修改失败");
    }
}
