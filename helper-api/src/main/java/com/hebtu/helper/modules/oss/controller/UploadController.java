package com.hebtu.helper.modules.oss.controller;

import com.hebtu.helper.common.exception.HelperException;
import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.oss.service.UploadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件上传
 * @author YKF
 * @date 2019/6/18 14:44
 */
@RequestMapping("/oss")
@RestController
public class UploadController {
    
    @Autowired
    private UploadService uploadService;

    /**
     * 上传文件
     */
    @RequestMapping("/upload")
    public H upload(@RequestParam("file") byte[] file,String fileName){
        if (file.length==0)
            throw new HelperException("上传文件不能为空");
        if(fileName.isEmpty())
            throw new HelperException("文件名不能为空");
        //上传文件
        String url ="http://"+ uploadService.uploadFile(fileName, file);
        return H.success().put("data", url);
    }

}
