package com.hebtu.helper.modules.goods.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.goods.entity.GoodsEntity;
import com.hebtu.helper.modules.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 二手市场
 * @author 刘肇
 * @date 2019/6/17 11：07
 */
@RequestMapping("/goods")
@RestController
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 插入商品
     * @param goodsEntity
     * @return
     */
    @RequestMapping("/save")
    public H save(GoodsEntity goodsEntity) {
        return goodsService.insertAPI(goodsEntity)>0?H.success():H.error(203,"插入失败");
    }

    /**
     * 修改商品
     * @param goodsEntity form表单传递过来的值
     * @return
     */
    @RequestMapping("/update")
    public H update(GoodsEntity goodsEntity){
        return goodsService.updateGoodsAPI(goodsEntity)>0?H.success():H.error(203,"修改失败");
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public H list(@RequestParam Map<String, Object> params){
        return goodsService.queryPageAPI(params);
    }

    /**
     * 用户已经上传的商品
     * @param sysUserId
     * @return
     */
    @RequestMapping("/userList/{sysUserId}")
    public H userList(@PathVariable("sysUserId") Long sysUserId) {
        return goodsService.selectBySysUserIdAPI(sysUserId);
    }

    /**
     * 擦亮
     * @param goodsId
     * @return
     */
    @RequestMapping("/updateReleaseDate/{goodsId}")
    public H updateReleaseDate(@PathVariable("goodsId") Long goodsId){
        return goodsService.updateReleaseDateAPI(goodsId)>0?H.success():H.error(203,"修改失败");
    }

    /**
     * 删除
     * @param goodsId
     * @return
     */
    @RequestMapping("/delete/{goodsId}")
    public H delete(@PathVariable("goodsId") Long goodsId){
        return goodsService.deleteGoodsAPI(goodsId)>0?H.success():H.error(203,"修改失败");
    }

    /**
     * 根据userId查询实体
     * @param
     * @return
     */
    @RequestMapping("/info/{goodsId}")
    public H info(@PathVariable("goodsId") Long goodsId){
        return goodsService.selectById(goodsId)!=null?H.success().put("data",goodsService.selectById(goodsId)):H.error(203,"未找到");
    }

}
