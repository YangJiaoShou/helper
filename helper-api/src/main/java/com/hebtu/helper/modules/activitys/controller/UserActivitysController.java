package com.hebtu.helper.modules.activitys.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.activities.entity.ActivitiesEntity;
import com.hebtu.helper.modules.activities.entity.UserActivitiesEntity;
import com.hebtu.helper.modules.activities.service.ActivitiesService;
import com.hebtu.helper.modules.activities.service.UserActivitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户校内活动
 */
@RequestMapping("/useractivitys")
@RestController
public class UserActivitysController {

    @Autowired
    private UserActivitiesService userActivitiesService;  //UserActivitiesService的实例
    private ActivitiesService activitiesService; //ActivitiesService的实例
    /**
     * 用户参加校内活动申请
     * @return
     */
    @RequestMapping("/apply")
    public H apply(UserActivitiesEntity userActivitiesEntity){
        userActivitiesEntity.setState(0);
        boolean isApply =userActivitiesService.insert(userActivitiesEntity);//利用CRUD接口完成实体数据添加到数据库
        return isApply?H.success("申请成功"):H.success("申请失败");//
    }

    /**
     * 根据userid用户id查询自己的报名信息
     * @param userid
     * @return
     */
    @RequestMapping("/selectmyself/{userid}")
    public H selectmyself(@PathVariable("userid")Long userid){
        Map<String,Object> map=new HashMap<>();
        List<UserActivitiesEntity>userActivitiesEntities=userActivitiesService.selectByMap(map);
        return H.success().put("data",userActivitiesEntities);
    }


    /**
     * 用户根据活动标题查询活动信息
     */
    @RequestMapping("/selecttitle/{title}")
    public H selecttitle(@PathVariable("title")String title){
        Map<String ,Object> map=new HashMap<>();
        map.put("state",1);
        map.put("title",title);
        List<ActivitiesEntity> activitiesEntities= activitiesService.selectByMap(map);
        System.out.println(activitiesEntities);
        return H.success().put("data", activitiesEntities);
    }
}
