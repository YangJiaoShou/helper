#! /bin/bash
# 更新代码 & 打包脚本
# 设置编码
export LANG=zh_CN.UTF-8

# 判断输入的参数是否合法

echo "输入要发布的服务 :" $1
if [ ! -n "$1" ]
then
    echo '缺少参数 : 发布的服务未指定,脚本退出!!!'
    exit 1
elif [ "$1" = "api" ]
    then
    echo "参数合法"
elif [ "$1" = "admin" ]
    then
    echo "参数合法"
else
    echo "服务参数不合法, 只能指 api 或者 admin"
    exit 1
fi
service=$1


# gitlab 更新工程
echo "更新  code";
cd /data/helper/code/helper
git clean -d -fx
git pull

# 打包编译
echo "maven打包编译...";

mvn clean package -Dmaven.test.skip=true

echo "stop ${service} ...";
sh /data/helper/server-${service}/${service}.sh stop


# 把jar包部署到运行目录

echo "copy ${service} 部署文件...";
rm -rf /data/helper/server-${service}/*
cp -rf /data/helper/code/helper/helper-${service}/target/helper-${service}.jar /data/helper/server-${service}/
cd /data/helper/server-${service}/
ln -s /data/helper/code/helper/helper-${service}/${service}.sh ${service}.sh
echo "copy ${service} 部署文件...done";


echo "start ${service} ...";
sh /data/helper/server-${service}/${service}.sh start

echo "  ${service}版本发布成功  "
