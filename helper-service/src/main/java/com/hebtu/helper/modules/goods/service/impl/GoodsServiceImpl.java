package com.hebtu.helper.modules.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.goods.dao.GoodsDao;
import com.hebtu.helper.modules.goods.entity.GoodsEntity;
import com.hebtu.helper.modules.goods.service.GoodsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("goodsService")
public class GoodsServiceImpl extends ServiceImpl<GoodsDao, GoodsEntity> implements GoodsService {

    @Override
    public Integer updateStates(Long goodId, Integer states) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setGoodId(goodId);
        if(states==0)
            goodsEntity.setState(1);
        else
            goodsEntity.setState(0);
        return baseMapper.updateById(goodsEntity);
    }

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String title = (String)params.get("title");
        Page<GoodsEntity> page = this.selectPage(
                new Query<GoodsEntity>(params).getPage(),
                new EntityWrapper<GoodsEntity>().
                        like(StringUtils.isNotBlank(title),"title", title).
                        orderBy("is_host",false)
        );
        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public H selectBySysUserIdAPI(Long sysUserId) {
        String id = String.valueOf(sysUserId);

        List<GoodsEntity> goodsEntities=this.selectList(new EntityWrapper<GoodsEntity>().
                like(StringUtils.isNotBlank(id),"sys_user_id", id).
                orderBy("is_host",false).
                eq("state",1).
                orderBy("release_date",true));
        return H.success().put("data", goodsEntities);
    }


    @Override
    public Integer updateIsHost(Long goodId, Integer isHost) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setGoodId(goodId);
        if(isHost==0)
            goodsEntity.setIsHost(1);
        else
            goodsEntity.setIsHost(0);
        return baseMapper.updateById(goodsEntity);
    }

    @Override
    public H queryPageAPI(Map<String, Object> params) {
        String goodId = (String)params.get("goodId");
        List<GoodsEntity> goodsEntities=this.selectList(new EntityWrapper<GoodsEntity>().
                like(StringUtils.isNotBlank(goodId),"good_id", goodId).
                orderBy("is_host",false).
                eq("state",1).
                orderBy("release_date",true));
        return H.success().put("data", goodsEntities);
    }

    @Override
    public Integer updateReleaseDateAPI(Long goodsId) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setGoodId(goodsId);
        goodsEntity.setReleaseDate(new Date());
        return baseMapper.updateById(goodsEntity);
    }


    @Override
    public Integer deleteGoodsAPI(Long goodsId) {
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setGoodId(goodsId);
        goodsEntity.setState(0);
        return baseMapper.updateById(goodsEntity);
    }

    @Override
    public Integer insertAPI(GoodsEntity goodsEntity) {
        return baseMapper.insert(goodsEntity);
    }

    @Override
    public Integer updateGoodsAPI(GoodsEntity goodsEntity) {
        return baseMapper.updateById(goodsEntity);
    }
}
