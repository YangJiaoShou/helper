package com.hebtu.helper.modules.job.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.job.dao.JobDao;
import com.hebtu.helper.modules.job.entity.JobEntity;
import com.hebtu.helper.modules.job.service.JobService;
import com.hebtu.helper.modules.sys.service.impl.SysRoleMenuServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service("jobService")
public class JobServiceImpl extends ServiceImpl<JobDao, JobEntity> implements JobService {

    private static final Logger log = LoggerFactory.getLogger(SysRoleMenuServiceImpl.class);

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String jobId=(String)params.get("jobId");
        String title=(String)params.get("title");
        Page<JobEntity> page = this.selectPage(
                new Query<JobEntity>(params).getPage(),
                new EntityWrapper<JobEntity>()
                        .eq(StringUtils.isNotBlank(jobId),"job_id", jobId)
                        .like(StringUtils.isNotBlank(title),"title", title)
        );
        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public boolean insert(JobEntity jobEntity) {
        try{
            baseMapper.insert(jobEntity);
        }catch (Exception e){
            log.info("发布失败:{}",jobEntity.getTitle());
            return false;
        }
        return true;
    }

    @Override
    public Integer updateStates(Long jobId, Integer states) {
        JobEntity jobEntity = new JobEntity();
        jobEntity.setJobId(jobId);
        if(states==0)
            jobEntity.setState(1);
        else
            jobEntity.setState(0);
        return baseMapper.updateById(jobEntity);
    }

    @Override
    public Integer updateIsHost(Long jobId, Integer isHost) {
        JobEntity jobEneity = new JobEntity();
        jobEneity.setJobId(jobId);
        if(isHost==0)
            jobEneity.setIsHost(1);
        else
            jobEneity.setIsHost(0);
        return baseMapper.updateById(jobEneity);
    }

    @Override
    public LayuiPage listJobAPI(Map<String, Object> params) {
        Page<JobEntity> page = this.selectPage(
                new Query<JobEntity>(params).getPage(),
                new EntityWrapper<JobEntity>()
                        .eq("state",1)
                        .ge("end_time",new Date())
        );
        return new LayuiPage(page.getRecords(),page.getTotal());
    }

    @Override
    public Integer deleteByJobIdAPI(Long jobId) {
        JobEntity jobEntity = new JobEntity();
        jobEntity.setJobId(jobId);
        jobEntity.setState(0);
        return baseMapper.updateById(jobEntity);
    }

}

