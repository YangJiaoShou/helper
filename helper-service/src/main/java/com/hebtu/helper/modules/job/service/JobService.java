package com.hebtu.helper.modules.job.service;

import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.job.entity.JobEntity;

import java.util.List;
import java.util.Map;

/**
 * 兼职
 * @author 小灰灰
 * @date 2019年6月1日 11点20分
 */

public interface JobService extends IService<JobEntity> {


    LayuiPage queryPage(Map<String, Object> params);

    /**
     * 增加条目
     * @param jobEntity   兼职实例
     * @return
     */
    boolean insert(JobEntity jobEntity);

    /**
     * 修改状态
     * @param jobId   兼职id
     * @param states   状态
     * @return
     */
    Integer updateStates(Long jobId,Integer states);

    /**
     * 修改状态
     * @param jobId   兼职id
     * @param isHost   热度
     * @return
     */
    Integer updateIsHost(Long jobId,Integer isHost);

    /**
     * 查询兼职信息API
     * @param params 参数表
     * @return
     */
    LayuiPage listJobAPI(Map<String, Object> params);

    /**
     * 删除兼职接口(未开放
     * @param jobId   兼职id
     * @return
     */
    Integer deleteByJobIdAPI(Long jobId);
}
