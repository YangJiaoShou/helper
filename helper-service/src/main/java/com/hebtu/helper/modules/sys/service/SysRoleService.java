package com.hebtu.helper.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.sys.entity.SysRoleEntity;

import java.util.Map;

/**
 * 角色
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    /**
     * 分页
     * @param params
     * @return
     */
    LayuiPage queryPage(Map<String, Object> params);

    /**
     * 保存角色及角色对应的菜单
     * @param sysRoleEntity
     */
    boolean save(SysRoleEntity sysRoleEntity);

    /**
     * 删除
     * @param roleId
     * @return
     */
    boolean delete(Long roleId);

    /**
     * 修改
     * @param sysRoleEntity
     * @return
     */
    void update(SysRoleEntity sysRoleEntity);

    /**
     * 根据角色查询
     * @param roleId
     * @return
     */
    SysRoleEntity queryByRoleId(Long roleId);
}

