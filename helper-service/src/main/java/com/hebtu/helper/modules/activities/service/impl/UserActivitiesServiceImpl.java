package com.hebtu.helper.modules.activities.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.activities.service.UserActivitiesService;
import com.hebtu.helper.modules.activities.dao.UserActivitiesDao;
import com.hebtu.helper.modules.activities.entity.UserActivitiesEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("userActivitiesService")
public class UserActivitiesServiceImpl extends ServiceImpl<UserActivitiesDao, UserActivitiesEntity> implements UserActivitiesService {

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        Page<UserActivitiesEntity> page = this.selectPage(
                new Query<UserActivitiesEntity>(params).getPage(),
                new EntityWrapper<UserActivitiesEntity>()
        );

        return new LayuiPage(page.getRecords(), page.getTotal());
    }

}
