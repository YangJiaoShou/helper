package com.hebtu.helper.modules.lost.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.lost.service.LostFindService;
import com.hebtu.helper.modules.lost.dao.LostFindDao;
import com.hebtu.helper.modules.lost.entity.LostFindEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("lostFindService")
public class LostFindServiceImpl extends ServiceImpl<LostFindDao, LostFindEntity> implements LostFindService {

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String title = (String)params.get("title");
        Page<LostFindEntity> page = this.selectPage(
                new Query<LostFindEntity>(params).getPage(),
                new EntityWrapper<LostFindEntity>()
                        .like(StringUtils.isNotBlank(title),"title", title).orderBy("audit")
        );
        return new LayuiPage(page.getRecords(), page.getTotal());
    }

   @Override
    public Integer updataStatus(Long userId, boolean status) {
        LostFindEntity lostFindEntity=new LostFindEntity();
        lostFindEntity.setUserId(userId);
        if(status==true)
            lostFindEntity.setState(1);
        else
            lostFindEntity.setState(0);

        return baseMapper.updateById(lostFindEntity);
    }


    /**
     * 分页显示丢失物品
     * @param params
     * @return
     */
    @Override
    public List<LostFindEntity> listLost(Map<String, Object> params) {
        String type = null;
        if(params.get("type")!=null)
            type=params.get("type").toString();
        Page<LostFindEntity> page = this.selectPage(
                new Query<LostFindEntity>(params).getPage(),
                new EntityWrapper<LostFindEntity>()
                .eq("state","1")
                .eq("audit","1")
                .eq(StringUtils.isNotBlank(type),"type",type)
        );
        return page.getRecords();
    }
    /**
     * 插入记录
     *
     */
    public Integer insertLostAPI(LostFindEntity lostFindEntity){

        return baseMapper.insert(lostFindEntity);
    }


    /**
     * 删除记录(把记录的state改为删除)
     * @param lostFindEntity
     * @return
     */
    public Integer delLostAPI(LostFindEntity lostFindEntity){

        lostFindEntity.setState(3);
        return baseMapper.updateById(lostFindEntity);
    }

    /**
     * 修改记录
     * @param lostFindEntity
     * @return
     */
    public Integer updateAPI(LostFindEntity lostFindEntity){

        lostFindEntity.setAudit(0);
        return baseMapper.updateById(lostFindEntity);
    }
}
