package com.hebtu.helper.modules.activities.service;


import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.activities.entity.UserActivitiesEntity;

import java.util.Map;

/**
 * 用户参加活动表)
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface UserActivitiesService extends IService<UserActivitiesEntity> {

    LayuiPage queryPage(Map<String, Object> params);
}

