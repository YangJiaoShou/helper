package com.hebtu.helper.modules.goods.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.modules.goods.dao.UserGoodsDao;
import com.hebtu.helper.modules.goods.entity.UserGoodsEntity;
import com.hebtu.helper.modules.goods.service.UserGoodsService;
import org.springframework.stereotype.Service;

@Service("userGoodsService")
public class UserGoodsServiceImpl extends ServiceImpl<UserGoodsDao, UserGoodsEntity> implements UserGoodsService {

}
