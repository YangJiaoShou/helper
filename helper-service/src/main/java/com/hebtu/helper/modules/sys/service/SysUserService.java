package com.hebtu.helper.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.sys.entity.SysUserEntity;

import java.util.Map;

/**
 * 后台用户
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface SysUserService extends IService<SysUserEntity> {

    /**
     * 保存用户
     * @param sysUserEntity
     * @return
     */
    boolean save(SysUserEntity sysUserEntity);

    /**
     * 根据账号查找实体
     * @param username
     * @return
     */
    SysUserEntity selectByUsername(String username);


    LayuiPage queryPage(Map<String, Object> params);

    /**
     * 修改状态
     * @param userId   用户id
     * @param status   状态
     * @return
     */
    Integer updataStatus(Long userId,boolean status);


    /**
     * 修改
     * @param sysUserEntity
     * @return
     */
    boolean update(SysUserEntity sysUserEntity);
}

