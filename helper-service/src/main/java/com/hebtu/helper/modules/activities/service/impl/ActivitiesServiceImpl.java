package com.hebtu.helper.modules.activities.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.activities.service.ActivitiesService;
import com.hebtu.helper.modules.activities.dao.ActivitiesDao;
import com.hebtu.helper.modules.activities.entity.ActivitiesEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("activitiesService")
public class ActivitiesServiceImpl extends ServiceImpl<ActivitiesDao, ActivitiesEntity> implements ActivitiesService {

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String userId=null;
        if(params.get("userId")!=null){
            userId=String.valueOf(params.get("userId"));  //拿到当前用户的id
        }
        String title=(String)params.get("title");  //标题
        Page<ActivitiesEntity> page = this.selectPage(
                new Query<ActivitiesEntity>(params).getPage(),
                new EntityWrapper<ActivitiesEntity>()
                        .ne("state",0)  //过滤掉删除状态的记录
                        .like(StringUtils.isNotBlank(title),"title",title)  //需要查询的活动标题
                        .eq(StringUtils.isNotBlank(userId),"sys_user_id",userId)  //判断当前用户id
        );

        return new LayuiPage(page.getRecords(), page.getTotal());
    }
     /**
     * 设置状态
     * @param id
     * @param states
     * @return
     */
    @Override
    public Integer updataStates(Long id, Integer states) {
        ActivitiesEntity activitiesEntity = new ActivitiesEntity();
        activitiesEntity.setId(id);
        if(states==1)
            activitiesEntity.setState(2);
        else
            activitiesEntity.setState(1);
        return baseMapper.updateById(activitiesEntity);
    }

    /**
     * 设置热度
     * @param id
     * @param isHost
     * @return
     */
    @Override
    public Integer updataIsHost(Long id, Integer isHost) {
        ActivitiesEntity activitiesEntity = new ActivitiesEntity();
        activitiesEntity.setId(id);
        System.out.println(id);
        if(isHost==0)
            activitiesEntity.setIsHost(1);
        else
            activitiesEntity.setIsHost(0);
        return baseMapper.updateById(activitiesEntity);
    }
}
