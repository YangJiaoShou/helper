package com.hebtu.helper.modules.sys.service;


import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.sys.entity.SysMenuEntity;

import java.util.List;


/**
 * 菜单
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface SysMenuService extends IService<SysMenuEntity> {

    /**
     * 根据用户id查询他所拥有的菜单
     * @param userId 用户id
     * @return
     */
    List<SysMenuEntity> list(Long userId);

    /**
     * 根据menuId删除
     * @param menuId
     */
    H delete(Long menuId);

    /**
     * 根据menuId查询所有子菜单
     * @param menuId
     */
    List<SysMenuEntity> queryAllChildByMenuId(Long menuId);
}

