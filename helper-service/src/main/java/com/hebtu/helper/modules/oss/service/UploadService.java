package com.hebtu.helper.modules.oss.service;


/**
 * 文件上传
 * @author YKF
 * @date 2019/6/17 10:08
 */
public interface UploadService {

    String  uploadFile(String fileName, byte[] file);
}
