package com.hebtu.helper.modules.oss.config;

/**
 * @author YKF
 * @date 2019/6/17 9:21
 */

public class OssConfig {
    //腾讯云绑定的域名
    public String qcloudDomain;
    //腾讯云路径前缀
    private String qcloudPrefix;
    //腾讯云AppId
    private Integer qcloudAppId;
    //腾讯云SecretId
    private String qcloudSecretId;
    //腾讯云SecretKey
    private String qcloudSecretKey;
    //腾讯云BucketName
    private String qcloudBucketName;
    //腾讯云COS所属地区
    private String qcloudRegion;

    public OssConfig(String qcloudDomain, String qcloudPrefix, Integer qcloudAppId, String qcloudSecretId, String qcloudSecretKey, String qcloudBucketName, String qcloudRegion) {
        this.qcloudDomain = qcloudDomain;
        this.qcloudPrefix = qcloudPrefix;
        this.qcloudAppId = qcloudAppId;
        this.qcloudSecretId = qcloudSecretId;
        this.qcloudSecretKey = qcloudSecretKey;
        this.qcloudBucketName = qcloudBucketName;
        this.qcloudRegion = qcloudRegion;
    }

    public String getQcloudDomain() {
        return qcloudDomain;
    }

    public String getQcloudPrefix() {
        return qcloudPrefix;
    }

    public Integer getQcloudAppId() {
        return qcloudAppId;
    }

    public String getQcloudSecretId() {
        return qcloudSecretId;
    }

    public String getQcloudSecretKey() {
        return qcloudSecretKey;
    }

    public String getQcloudBucketName() {
        return qcloudBucketName;
    }

    public String getQcloudRegion() {
        return qcloudRegion;
    }
}
