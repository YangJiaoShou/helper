package com.hebtu.helper.modules.job.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.job.dao.UserJobDao;
import com.hebtu.helper.modules.job.entity.JobEntity;
import com.hebtu.helper.modules.job.entity.UserJobEntity;
import com.hebtu.helper.modules.job.service.JobService;
import com.hebtu.helper.modules.job.service.UserJobService;
import com.hebtu.helper.modules.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userJobService")
public class UserJobServiceImpl extends ServiceImpl<UserJobDao, UserJobEntity> implements UserJobService {
    @Autowired
    JobService jobService;

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        String jobId=params.get("jobId").toString();
        String stNo=params.get("stNo").toString();
        Page<UserJobEntity> page = this.selectPage(
            new Query<UserJobEntity>(params).getPage(),
            new EntityWrapper<UserJobEntity>()
                    .eq(StringUtils.isNotBlank(jobId),"job_id",jobId)
                    .eq(StringUtils.isNotBlank(stNo),"st_no",stNo)
        );
        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public LayuiPage queryPageByJobId(Map<String, Object> params) {
        String jobId=(String)params.get("jobId");
        if(jobId==null)return new LayuiPage(null,0);
        Page<UserJobEntity> page = this.selectPage(
                new Query<UserJobEntity>(params).getPage(),
                new EntityWrapper<UserJobEntity>()
                        .eq("job_id",jobId)
                        .eq("state",1)
        );
        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public Integer deleteUserJobAPI(UserJobEntity userJobEntity) {
        userJobEntity.setState(0);
        return baseMapper.updateById(userJobEntity);
    }

    @Override
    public LayuiPage queryPageByStNoAPI(Map<String, Object> params,Long stNo) {
        List<UserJobEntity> result= baseMapper.selectList(
                new EntityWrapper<UserJobEntity>()
                    .eq("st_no",stNo)
                    .eq("state",1)
        );
        List<Long> list=new ArrayList<>(result.size());
        for(UserJobEntity j:result){
            list.add(j.getJobId());
        }
        if(list.size()==0){
            return new LayuiPage(null,0);
        }
        Page<JobEntity> page = jobService.selectPage(
                new Query<JobEntity>(params).getPage(),
                new EntityWrapper<JobEntity>()
                    .in("job_id",list)
                    .eq("state",1)
        );
        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    @Override
    public Integer updateUserJobAPI(UserJobEntity userJobEntity) {
        userJobEntity.setState(1);
        return baseMapper.update(
                userJobEntity,
                new EntityWrapper<UserJobEntity>()
                    .eq("state",1)      //只允许更新state为1的
                    .eq("st_no",userJobEntity.getStNo())
        );
    }

    @Override
    public boolean insertUserJobAPI(UserJobEntity userJobEntity) {

        return this.insert(userJobEntity);
    }

    @Override
    public Map<Long, Map<String, Object>> queryByStNoAPI(Long stNo) {
        List<Map<String,Object>> result= baseMapper.selectMaps(
                new EntityWrapper<UserJobEntity>()
                        .eq("st_no",stNo)
                        .eq("state",1)
        );


        List<Long> list=new ArrayList<>(result.size());

        for(Map<String,Object>mp:result){
            list.add((Long)mp.get("jobId"));
        }

        List<Map<String,Object>> page = jobService.selectMaps(
                new EntityWrapper<JobEntity>()
                        .in("job_id",list)
                        .eq("state",1)
        );
        Map<Long,Map<String,Object>> ans=new HashMap<>();

        for(Map<String,Object> mp:page){
            ans.put((Long)mp.get("jobId"),mp);
        }
        for(Map<String,Object> mp:result){
            if(ans.containsKey((Long)mp.get("jobId"))){
                Map<String,Object> kid=ans.get((Long)mp.get("jobId"));
                kid.putAll(mp);
            }
        }
        return ans;
    }


}
