package com.hebtu.helper.modules.lost.service;


import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.lost.entity.LostFindEntity;

import java.util.List;
import java.util.Map;

/**
 * 失物招领
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface LostFindService extends IService<LostFindEntity> {

    LayuiPage queryPage(Map<String, Object> params);

    Integer updataStatus(Long userId,boolean status);

    /**
     * 丢失物品分页
     * @param params
     * @return
     */
    List<LostFindEntity> listLost(Map<String, Object> params);

    /**
     * 插入记录
     * @param lostFindEntity
     * @return
     */
    Integer insertLostAPI(LostFindEntity lostFindEntity);


    /**
     * 删除记录
     * @param id
     * @return
     */
    Integer delLostAPI(LostFindEntity lostFindEntity);

    /**
     * 修改记录
     * @param lostFindEntity
     * @return
     */
    Integer updateAPI(LostFindEntity lostFindEntity);
}

