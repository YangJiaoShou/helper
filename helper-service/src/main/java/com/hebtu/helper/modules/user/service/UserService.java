package com.hebtu.helper.modules.user.service;


import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.user.entity.UserEntity;

import java.util.Map;

/**
 * c端用户
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface UserService extends IService<UserEntity> {

    LayuiPage queryPage(Map<String, Object> params);

    /**
     * 根据账号查询用户
     * @param stNo
     * @return  查询到的实体
     */
    UserEntity selectByStNo(String stNo);
}

