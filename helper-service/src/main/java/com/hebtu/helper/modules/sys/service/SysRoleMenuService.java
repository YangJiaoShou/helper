package com.hebtu.helper.modules.sys.service;


import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.modules.sys.entity.SysRoleMenuEntity;

import java.util.List;


/**
 * 角色对应菜单
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

    /**
     * 根据角色查询所有菜单id
     * @param roles  角色id
     * @return
     */
    List<Long> queryMenuIdByRoleId(List<Long> roles);

    /**
     * 保存角色对应的菜单信息
     * @param roleId 角色id
     * @param menuIds 菜单id
     */
    boolean save(Long roleId, List<Long> menuIds);

    /**
     * 根据角色id删除
     * @param roleId
     * @return
     */
    boolean deleteByRoleId(Long roleId);
}

