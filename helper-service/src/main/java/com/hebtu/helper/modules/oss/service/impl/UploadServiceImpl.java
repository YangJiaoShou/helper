package com.hebtu.helper.modules.oss.service.impl;

import com.hebtu.helper.modules.oss.config.OssUtile;
import com.hebtu.helper.modules.oss.service.UploadService;
import org.springframework.stereotype.Service;

/**
 *文件上传
 * @author YKF
 * @date 2019/6/17 10:09
 */
@Service("uploadService")
public class UploadServiceImpl implements UploadService {


    @Override
    public String  uploadFile(String fileName, byte[] file) {
        OssUtile ossUtile=new OssUtile();
        String url=ossUtile.uploadSuffix(file, fileName);
        return url;
    }
}
