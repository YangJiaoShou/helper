package com.hebtu.helper.modules.goods.service;

import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.goods.entity.GoodsEntity;

import java.util.Map;

public interface GoodsService extends IService<GoodsEntity> {
    /**
     * 修改状态
     * @param goodId   商品id
     * @param states   状态
     * @return
     */
    Integer updateStates(Long goodId,Integer states);

    /**
     * 修改状态
     * @param goodId   商品id
     * @param isHost   热度
     * @return
     */
    Integer updateIsHost(Long goodId,Integer isHost);

    LayuiPage queryPage(Map<String, Object> params);

    /**
     * 根据账号查找该用户的所有货物
     * @param sysUserId
     * @return
     */
    H selectBySysUserIdAPI(Long sysUserId);

    /**
     * API的分页显示方法，按照热度、擦亮时间排序，及不显示已删除的货物
     * @param params
     * @return
     */
    H queryPageAPI(Map<String, Object> params);

    /**
     * 擦亮方法的API
     * @param goodsId
     * @return
     */
    Integer updateReleaseDateAPI(Long goodsId);

    /**
     * 更新整个货物的信息
     * @param goodsEntity
     * @return
     */
    Integer updateGoodsAPI(GoodsEntity goodsEntity);

    /**
     * 删除方法的API
     * @param goodsId
     * @return
     */
    Integer deleteGoodsAPI(Long goodsId);

    /**
     * 添加新货物
     * @param goodsEntity
     * @return
     */
    Integer insertAPI(GoodsEntity goodsEntity);
}
