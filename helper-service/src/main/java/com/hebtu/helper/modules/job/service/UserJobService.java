package com.hebtu.helper.modules.job.service;

import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.job.entity.JobEntity;
import com.hebtu.helper.modules.job.entity.UserJobEntity;

import java.util.Map;

/**
 * 用户兼职表
 * @author 小灰灰
 * @date 2019年6月1日 11点20分
 */

public interface UserJobService extends IService<UserJobEntity> {

    LayuiPage queryPage(Map<String, Object> params);

    /**
     * 查询报名信息（从后台，查看报名者
     * @param params   参数表
     * @return LayuiPage<UserJobEntity>
     */
    LayuiPage queryPageByJobId(Map<String, Object> params);

    /**
     * 取消报名
     * @param id  参数表
     * @return 影响行数
     * @apiNote
     **/
    Integer deleteUserJobAPI(UserJobEntity userJobEntity);

    /**
     * 通过学号查询当前报名的兼职信息
     * @param stNo 学号
     * @return LayuiPage<JobEntity>
     * @apiNote
     */
    LayuiPage queryPageByStNoAPI(Map<String, Object> params,Long stNo);

//    /**
//     * 通过学号和兼职ID删除报名信息（预留，暂时不实现，考虑安全问题
//     * @param stNo 学号
//     * @param jobId 兼职id
//     * @return Integer 影响条目
//     * @apiNote
//     */
//    Integer deleteByStNoAndJobIdAPI(Long stNo,Long jobId);
    /**
     * 更新报名信息
     * @param userJobEntity 报名信息
     * @return Integer 影响条目
     * @apiNote
     */
    Integer updateUserJobAPI(UserJobEntity userJobEntity);

    /**
     * 添加报名信息
     * @param userJobEntity 报名信息
     * @return boolean 影响条目
     * @apiNote
     */
     boolean insertUserJobAPI(UserJobEntity userJobEntity);

    /**
     * 查询已报名的兼职
     * @param  stNo  报名信息
     * @return Map<String,Map<String,String>> 影响条目
     * @apiNote
     */
    Map<Long,Map<String,Object>> queryByStNoAPI(Long stNo);
}
