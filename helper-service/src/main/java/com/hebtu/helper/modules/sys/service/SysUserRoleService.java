package com.hebtu.helper.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.modules.sys.entity.SysUserRoleEntity;

import java.util.List;


/**
 * 角色与用户对应关系
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {

    /**
     * 根据用户ID查询他的所有角色id
     * @param userId 用户id
     * @return
     */
    List<Long> queryRolesByUserID(Long userId);

    Integer saveAll(Long userId,List<Long> roleIds);

    void update(Long userId,List<Long> roleIds);

    Integer delete(Long userId);
}

