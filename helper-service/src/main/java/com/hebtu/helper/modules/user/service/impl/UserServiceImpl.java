package com.hebtu.helper.modules.user.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.common.utils.Query;
import com.hebtu.helper.modules.user.service.UserService;
import com.hebtu.helper.modules.user.dao.UserDao;
import com.hebtu.helper.modules.user.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public LayuiPage queryPage(Map<String, Object> params) {
        Page<UserEntity> page = this.selectPage(
                new Query<UserEntity>(params).getPage(),
                new EntityWrapper<UserEntity>()
        );

        return new LayuiPage(page.getRecords(), page.getTotal());
    }

    /**
     * 根据账号查询用户
     * @param stNo
     * @return  查询到的实体
     */
    @Override
    public UserEntity selectByStNo(String stNo) {
        return baseMapper.selectByStNo(stNo);
    }

}
