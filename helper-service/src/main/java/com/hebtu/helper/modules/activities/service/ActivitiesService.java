package com.hebtu.helper.modules.activities.service;


import com.baomidou.mybatisplus.service.IService;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.activities.entity.ActivitiesEntity;

import java.util.Map;

/**
 * 活动
 * @author YKF
 * @date 2019-05-22 22:16:33
 */
public interface ActivitiesService extends IService<ActivitiesEntity> {

    /**
     *
     * @param params
     * @return
     */
    LayuiPage queryPage(Map<String, Object> params);
    Integer updataStates(Long id, Integer states) ;
    Integer updataIsHost(Long id, Integer isHost) ;
}

