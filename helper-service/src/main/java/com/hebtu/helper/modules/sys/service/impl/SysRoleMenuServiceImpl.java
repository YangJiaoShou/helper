package com.hebtu.helper.modules.sys.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hebtu.helper.modules.sys.service.SysRoleMenuService;
import com.hebtu.helper.modules.sys.dao.SysRoleMenuDao;
import com.hebtu.helper.modules.sys.entity.SysRoleMenuEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao, SysRoleMenuEntity> implements SysRoleMenuService {

    private static final Logger log = LoggerFactory.getLogger(SysRoleMenuServiceImpl.class);

    @Override
    public List<Long> queryMenuIdByRoleId(List<Long> roles) {
        return baseMapper.queryMenuIdByRoleId(roles);
    }

    @Override
    public boolean save(Long roleId, List<Long> menuIds) {
        try{
            for (Long menuId : menuIds) {
                SysRoleMenuEntity sysRoleMenuEntity=new SysRoleMenuEntity();
                sysRoleMenuEntity.setRoleId(roleId);
                sysRoleMenuEntity.setMenuId(menuId);
                baseMapper.insert(sysRoleMenuEntity);
            }
        }catch (Exception e){
            log.info("角色绑定菜单出错：角色id{}",roleId);
            return false;
        }
        return true;
    }

    /**
     * 根据roleId删除
     * @param roleId
     * @return
     */
    @Override
    public boolean deleteByRoleId(Long roleId) {
        Map<String, Object> map = new HashMap<>();
        map.put("role_id", roleId);
        return baseMapper.deleteByMap(map)>0?true:false;
    }
}
