package com.hebtu.helper.exception;
import com.hebtu.helper.common.exception.HelperException;
import com.hebtu.helper.common.utils.H;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局捕获异常信息
 * @author YKF
 * @date 2019/5/19 16:45
 */
@RestControllerAdvice
public class HelperExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(HelperExceptionHandler.class);

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(HelperException.class)
    public H handleException(HelperException e) {
        H h = new H();
        h.put("code", e.getCode());
        h.put("msg", e.getMessage());
        return h;
    }

    /**
     * 500异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public H  handleException(Exception e) {
        log.error(e.getMessage(), e);
        return H.error();
    }
}
