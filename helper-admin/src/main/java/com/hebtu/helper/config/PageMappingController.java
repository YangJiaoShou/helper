package com.hebtu.helper.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 页面映射
 * @author YKF
 * @date 2019/5/19 16:36
 */
@Controller
public class PageMappingController {

    @RequestMapping("modules/{module}/{url}.html")
    public String module(@PathVariable("module") String module, @PathVariable("url") String url){
        return "modules/" + module + "/" + url;
    }

    @RequestMapping(value = {"/", "index.html"})
    public String index(){
        return "index";
    }

    @RequestMapping("login.html")
    public String login(){
        return "login";
    }
}
