package com.hebtu.helper.config;

import com.hebtu.helper.interceptor.AuthInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.LinkedList;
import java.util.List;


/**
 * @author YKF
 * @date 2019/5/19 16:38
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    private static final Logger log = LoggerFactory.getLogger(WebConfig.class);

    /**
     * 设置静态资源
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("========= 设置静态资源 =========");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }

    /**
     * 设置拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("========= 设置拦截器 =========");

        List<String> list = new LinkedList<>();
        list.add("/error.html");
        list.add("/login.html");
        list.add("/captcha.jpg");
        list.add("/sys/login");
        list.add("/static/**");
        registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/**").excludePathPatterns(list);
    }
}