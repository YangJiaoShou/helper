package com.hebtu.helper.config;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *mybatis分页配置
 * @author YKF
 * @date 2019/5/19 16:35
 */
@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = {"com.hebtu.helper.modules.*.dao"})
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
