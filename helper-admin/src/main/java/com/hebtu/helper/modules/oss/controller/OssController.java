package com.hebtu.helper.modules.oss.controller;

import com.hebtu.helper.common.exception.HelperException;
import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.oss.service.UploadService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author YKF
 * @date 2019/6/15 18:57
 */
@RequestMapping("/oss")
@RestController
public class OssController {

    @Autowired
    private UploadService uploadService;


    /**
     * 上传文件
     */
    @RequestMapping("/upload")
    public H upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new HelperException("上传文件不能为空");
        }
        //上传文件
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        String url ="http://"+ uploadService.uploadFile(extension, file.getBytes());
        return H.success().put("data", url);
    }
}
