package com.hebtu.helper.modules.job.controller;

import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.job.entity.JobEntity;
import com.hebtu.helper.modules.job.service.UserJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/userJob")
public class UserJobController {
    @Autowired
    private UserJobService userJobService;

    /**
     * 通过jobId查询学生信息
     * @param jobId
     * @return
     */
    @RequestMapping("/queryByJobId/{jobId}")
    public LayuiPage queryStudentByJobId(@PathVariable("jobId") String jobId,@RequestParam Map<String, Object> params){
        params.put("jobId",jobId);
        return userJobService.queryPageByJobId(params);
    }
}
