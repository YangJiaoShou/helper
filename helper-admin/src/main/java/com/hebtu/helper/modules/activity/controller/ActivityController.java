package com.hebtu.helper.modules.activity.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.activities.entity.ActivitiesEntity;
import com.hebtu.helper.modules.activities.service.ActivitiesService;
import com.hebtu.helper.modules.cache.controller.AbstractController;
import com.hebtu.helper.modules.sys.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;
@RestController
@RequestMapping("/activitys")
public class ActivityController extends AbstractController {//

    @Autowired
    private ActivitiesService activitiesService;


    /**
     * 列表，得到用户的id在搜索功能时得到该值
     * @param params
     * @return
     */
    @RequestMapping("/list")
    public LayuiPage list(HttpSession httpSession,@RequestParam Map<String, Object> params){
        Long userid=getUserId(httpSession);
        if(userid!=1){
            params.put("userId",userid);
        }
        return activitiesService.queryPage(params);
    }

    /**
     * 保存
     * @param activitiesEntity
     * @return
     */
    @RequestMapping("/save")
    public H save(HttpSession httpSession, @RequestBody ActivitiesEntity activitiesEntity){  //把form表单封装到实体中

        SysUserEntity sysUserEntity= getUser(httpSession);
        activitiesEntity.setSysUserId(sysUserEntity.getUserId());
        activitiesEntity.setSysUserName(sysUserEntity.getNickName());

        boolean isSave =activitiesService.insert(activitiesEntity);  //利用CRUD接口完成实体数据添加到数据库
        return isSave?H.success("添加成功"):H.success("添加失败");  //
    }

    /**
     * 根据id查询实体,将记录添加到表单进行编辑修改
     * @param id
     * @return
     */
    @RequestMapping("/info/{id}")
    public H info(@PathVariable("id")Long id){
        ActivitiesEntity activitiesEntity=activitiesService.selectById(id);
        return activitiesEntity!=null?H.success().put("data",activitiesEntity):H.error(203,"未找到");
    }

    /**
     * 更新
     * @param activitiesEntity
     * @return
     */
    @RequestMapping("/update")
    public H update(@RequestBody ActivitiesEntity activitiesEntity){
        boolean isUpdate =activitiesService.updateById(activitiesEntity);
        return isUpdate?H.success("更新成功"):H.success("更新失败");//
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @RequestMapping("/delmsg/{id}")
    @Transactional
    public H deleteById(@PathVariable("id")Long id){
        ActivitiesEntity activitiesEntity= new ActivitiesEntity();  //逻辑删除记录
        activitiesEntity.setId(id);
        activitiesEntity.setState(0);
        boolean isDelete =activitiesService.updateById(activitiesEntity);
        return isDelete?H.success("删除成功"):H.error(230,"删除失败");
    }
    /**
     * 修改状态
     * @param id
     * @param state
     * @return
     */
    @RequestMapping("/state")
    public H states(Long id,Integer state) {
        return activitiesService.updataStates(id,state)==1? H.success("修改成功") : H.error(230,"修改失败");
    }

    /**
     * 修改热度
     * @param id
     * @param isHost
     * @return
     */
    @RequestMapping("/isHost")
    public H isHost(Long id,Integer isHost) {
        return activitiesService.updataIsHost(id,isHost)==1? H.success("修改成功") : H.error(230,"修改失败");
    }

}
