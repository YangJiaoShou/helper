package com.hebtu.helper.modules.cache.controller;

import com.hebtu.helper.common.constants.Constant;
import com.hebtu.helper.modules.sys.entity.SysUserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;

/**
 * 提取session
 * @author YKF
 * @date 2019/5/24 17:11
 */
public class AbstractController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected SysUserEntity getUser(HttpSession httpSession) {
        return (SysUserEntity)httpSession.getAttribute(Constant.SYS_KEY);
    }

    protected Long getUserId(HttpSession httpSession) {
        return getUser(httpSession).getUserId();
    }
}
