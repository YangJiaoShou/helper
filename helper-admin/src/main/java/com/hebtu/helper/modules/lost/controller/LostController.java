package com.hebtu.helper.modules.lost.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.lost.entity.LostFindEntity;
import com.hebtu.helper.modules.lost.service.LostFindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
@RestController
@RequestMapping("/lost")
public class LostController {

    @Autowired
    private LostFindService lostFindService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    public LayuiPage list(@RequestParam Map<String, Object> params){
        return lostFindService.queryPage(params);
    }

    /**
     * 保存
     * @param lostFindEntity
     * @return
     */
    @RequestMapping("/save")
    public H save(@RequestBody LostFindEntity lostFindEntity) {
        return lostFindService.insert(lostFindEntity)?H.success("插入成功") : H.error(230,"插入失败");
    }

    /**
     * 根据userId查询实体
     * @param userId
     * @return
     */
    @RequestMapping("/info/{userId}")
    public H info(@PathVariable("userId") Long userId){
        LostFindEntity lostFindEntity=lostFindService.selectById(userId);
        return lostFindService.selectById(lostFindEntity)!=null?H.success().put("data",lostFindService.selectById(lostFindEntity)):H.error(203,"未找到");
    }

    /**
     * 修改
     * @param lostFindEntity form表单传递过来的值
     * @return
     */
    @RequestMapping("/update")
    public H update(@RequestBody LostFindEntity lostFindEntity){
        return lostFindService.updateById(lostFindEntity)?H.success():H.error(203,"修改失败");
    }
    /**
     * 根据userId删除
     * @param userId
     * @return
     */
    @RequestMapping("/deleteById/{userId}")
    public H deleteById(@PathVariable("userId") Long  userId){
        return lostFindService.deleteById(userId)? H.success("删除成功"):H.error(230,"删除失败");
    }

    /**
     * 修改状态
     * @param userId
     * @param status
     * @return
     */
    @RequestMapping("/status")
    public H status(Long userId,boolean status) {
        return lostFindService.updataStatus(userId,status)==1? H.success("修改成功") : H.error(230,"修改失败");
    }


}
