package com.hebtu.helper.modules.sys.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.modules.cache.controller.AbstractController;
import com.hebtu.helper.modules.sys.entity.SysMenuEntity;
import com.hebtu.helper.modules.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author YKF
 * @date 2019/5/24 16:28
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 查询用户所拥有的菜单
     * @param httpSession
     * @return
     */
    @RequestMapping("/list")
    public H list(HttpSession httpSession) {
        return sysMenuService.list(getUserId(httpSession))==null? H.error("无菜单") : H.success().put("data",sysMenuService.list(getUserId(httpSession)));
    }

    /**
     * 保存菜单
     * @param sysMenuEntity
     * @return
     */
    @RequestMapping("/save")
    public H save(@RequestBody SysMenuEntity sysMenuEntity) {
        if(sysMenuEntity.getParentId()==0)
            sysMenuEntity.setUrl(null);
        return sysMenuService.insert(sysMenuEntity)?H.success():H.error(203,"插入失败");
    }

    /**
     * 根据menuid查询
     * @param menuId
     * @return
     */
    @RequestMapping("info/{menuId}")
    public H info(@PathVariable("menuId") Long menuId) {
        SysMenuEntity sysMenuEntity=sysMenuService.selectById(menuId);
        return sysMenuEntity!= null?H.success().put("data",sysMenuEntity):H.error(203,"查询错误");
    }

    /**
     * 修改菜单
     * @param sysMenuEntity
     * @return
     */
    @RequestMapping("/update")
    public H update(@RequestBody SysMenuEntity sysMenuEntity) {
        if(sysMenuEntity.getParentId()==0)
            sysMenuEntity.setUrl(null);
        return sysMenuService.updateById(sysMenuEntity)?H.success():H.error(203,"修改失败");
    }


    /**
     * 根据主键删除
     * @param menuId
     * @return
     */
    @RequestMapping("/delete/{menuId}")
    public H delete(@PathVariable("menuId") Long menuId) {
        return sysMenuService.delete(menuId);
    }
}
