package com.hebtu.helper.modules.goods.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.cache.controller.AbstractController;
import com.hebtu.helper.modules.goods.entity.GoodsEntity;
import com.hebtu.helper.modules.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/goods")
public class GoodsController extends AbstractController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public LayuiPage list(@RequestParam Map<String, Object> params){
        return goodsService.queryPage(params);
    }


    /**
     * 保存商品
     * @param goodsEntity
     * @return
     */
    @RequestMapping("/save")
    public H save(@RequestBody GoodsEntity goodsEntity) {
        return goodsService.insert(goodsEntity)?H.success("保存成功"):H.error(203,"插入失败");
    }

    /**
     * 修改
     * @param goodsEntity form表单传递过来的值
     * @return
     */
    @RequestMapping("/update")
    public H update(@RequestBody GoodsEntity goodsEntity){
        return goodsService.updateById(goodsEntity)?H.success():H.error(203,"修改失败");
    }

    /**
     * 修改状态
     * @param goodId
     * @param state
     * @return
     */
    @RequestMapping("/state")
    public H states(Long goodId,Integer state) {
        return goodsService.updateStates(goodId,state)==1? H.success("修改成功") : H.error(230,"修改失败");
    }

    /**
     * 修改热度
     * @param goodId
     * @param isHost
     * @return
     */
    @RequestMapping("/isHost")
    public H isHost(Long goodId,Integer isHost) {
        return goodsService.updateIsHost(goodId,isHost)==1? H.success("修改成功") : H.error(230,"修改失败");
    }

    /**
     * 根据userId删除
     * @param goodId
     * @return
     */
    @RequestMapping("/deleteById/{goodId}")
    public H deleteById(@PathVariable("goodId") Long  goodId){
        return goodsService.deleteById(goodId)? H.success("删除成功"):H.error(230,"删除失败");
    }

    /**
     * 根据goodsId查询GoodsEntity实体
     * @param goodId
     * @return
     */
    @RequestMapping("/info/{goodId}")
    public H info(@PathVariable("goodId") Long goodId){
        GoodsEntity goodsEntity=goodsService.selectById(goodId);
        return goodsService.selectById(goodsEntity)!=null?H.success().put("data",goodsService.selectById(goodsEntity)):H.error(203,"未找到");
    }

}
