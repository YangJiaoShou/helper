package com.hebtu.helper.modules.job.controller;


import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.cache.controller.AbstractController;
import com.hebtu.helper.modules.job.entity.JobEntity;
import com.hebtu.helper.modules.job.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/job")
public class JobController extends AbstractController {
    @Autowired
    private JobService jobService;

    @RequestMapping("/list")
    public LayuiPage list(@RequestParam Map<String, Object> params){
        return jobService.queryPage(params);
    }

    @RequestMapping("/save")
    public H save(HttpSession httpSession, @RequestBody JobEntity jobEntity){
        if(jobEntity.getTitle()==null){
            return H.error("标题不能为空");
        }
        if(jobEntity.getDescribe()==null){
            return H.error("描述不能为空");
        }
        if(jobEntity.getWage()==null){
            return H.error("工资不能为空");
        }
        if(jobEntity.getAddress()==null){
            return H.error("地址不能为空");
        }
        if(jobEntity.getContent()==null){
            return H.error("内容不能为空");
        }
        if(jobEntity.getActivitiesTime()==null){
            return H.error("开始时间不能为空");
        }
        if(jobEntity.getEndTime()==null){
            return H.error("结束时间不能为空");
        }
//        if(jobEntity.getEndTime().before(new Date())){
//            return H.error("已经结束了？？？");
//        }
        if(jobEntity.getTotal()==null){
            return H.error("可参与人数不能为空");
        }
//        jobEntity.setSysUserName();
        jobEntity.setTotalNow(0L);
        jobEntity.setSysUserId(getUserId(httpSession));
        jobEntity.setSysUserName(getUser(httpSession).getUsername());
        if(jobService.insert(jobEntity)){
            return H.success("发布成功");
        }else{
            return H.success("发布失败");
        }
    }

    @RequestMapping("/update")
    public H update(@RequestBody JobEntity jobEntity){
        //service为实现
        return jobService.updateById(jobEntity)?H.success():H.error(203,"修改失败");
    }

    /**
     * 修改状态
     * @param jobId
     * @param isHost
     * @return
     */
    @RequestMapping("/isHost")
    public H isHost(Long jobId,Integer isHost) {
        return jobService.updateIsHost(jobId,isHost)==1? H.success("修改成功") : H.error(230,"修改失败");
    }
    /**
     * 修改状态
     * @param jobId
     * @param state
     * @return
     */
    @RequestMapping("/state")
    public H states(Long jobId,Integer state) {
        return jobService.updateStates(jobId,state)==1? H.success("修改成功") : H.error(230,"修改失败");
    }

    /**
     * 根据jobId删除
     * @param jobId
     * @return
     */
    @RequestMapping("/deleteById/{jobId}")
    public H deleteById(@PathVariable("jobId") Long jobId){
        return jobService.deleteById(jobId)?H.success("修改成功"):H.error(230,"修改失败");
    }

    /**
     * 通过jobId查询job实例
     * @param jobId
     * @return
     */
    @RequestMapping("/info/{jobId}")
    public H info(@PathVariable("jobId") Long goodId){
        JobEntity jobEntity=jobService.selectById(goodId);
        return jobService.selectById(jobEntity)!=null?H.success().put("data",jobService.selectById(jobEntity)):H.error(203,"未找到");
    }
}
