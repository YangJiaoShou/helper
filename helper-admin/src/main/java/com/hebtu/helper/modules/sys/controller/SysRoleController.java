package com.hebtu.helper.modules.sys.controller;

import com.hebtu.helper.common.utils.H;
import com.hebtu.helper.common.utils.LayuiPage;
import com.hebtu.helper.modules.sys.entity.SysRoleEntity;
import com.hebtu.helper.modules.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

/**
 * 角色
 * @author YKF
 * @date 2019/5/27 13:47
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public LayuiPage list(@RequestParam Map<String, Object> params){
        return sysRoleService.queryPage(params);
    }

    /**
     * 保存角色
     * @param sysRoleEntity
     * @return
     */
    @RequestMapping("/save")
    @Transactional
    public H save(@RequestBody SysRoleEntity sysRoleEntity) {
        return sysRoleService.save(sysRoleEntity)?H.success("保存成功"):H.error(203,"保存失败");
    }

    /**
     * 根据角色ID查询
     * @param roleId
     * @return
     */
    @RequestMapping("/info/{roleId}")
    public H info(@PathVariable("roleId") Long roleId) {
        return H.success().put("data", sysRoleService.queryByRoleId(roleId));
    }

    /**
     * 修改
     * @param sysRoleEntity
     * @return
     */
    @RequestMapping("/update")
    @Transactional
    public H update(@RequestBody SysRoleEntity sysRoleEntity) {
        sysRoleService.update(sysRoleEntity);
        return H.success();
    }

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    @RequestMapping("/delete/{roleId}")
    @Transactional
    public H delete(@PathVariable("roleId") Long roleId){
        return sysRoleService.delete(roleId)?H.success("删除成功"):H.error(203,"删除失败");
    }
}
