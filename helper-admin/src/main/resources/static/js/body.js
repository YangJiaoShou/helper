$(function () {

    loadLeftMenu();
})
/****************前端做菜单业务处理，减轻服务器压力。菜单只有两级************************/
function loadLeftMenu() {
    $.ajax({
        url:"/sys/menu/list",
        type:"GET",
        contentType: "application/json",
        success:function (r) {
            if (r.code == 0) {
                var html=[];
                findParent(r.data,html)
                //join把数组转化为字符串
                $("#sysMenuList").html(html.join(''))
            }else{
                layer.msg(r.msg, {
                    icon: 2,
                    shade: 0.3,
                    time: 1000
                });
            }
        }
    })
}


/***********遍历一级菜单**************/
function findParent(data,html) {
    $.each(data,function (index,value) {
        if(value.parentId==0){
            html.push('<li class="layui-nav-item layui-nav-itemed">');
            html.push('<a class="layui-icon '+value.icon+'" style="font-size: 20px;"  href="javascript:;">'+value.name+'</a>');
            html.push('<dl class="layui-nav-child">');
            findChild(data,html,value.menuId);
            html.push('</dl>');
            html.push('</li>')
        }
    });
}
/***************遍历二级菜单****************/
function findChild(data,html,pid) {
    $.each(data,function (index,value) {
       if(value.parentId==pid){
           html.push('<dd><a class="layui-icon '+value.icon+'"  href="'+value.url+'">'+value.name+'</a></dd>')
       }
    });
}