var layuiTable; //定义变量
$(function () {
    // 加载表格数据
    layuiTable=layui.table.render({  //对变量layuiTable进行赋值
        elem: '#tableData'
        , url: '/activitys/list/'  //对应接口地址
        ,toolbar: '#toolbarDemo'
        , cols: [[
            {checkbox: true, fixed: true}
            , {field: 'id', title: 'ID', width: 100, unresize: true, sort: true, hide:true}
            , {field: 'title', title: '标题'}
            , {field: 'describe', title: '描述'}
            , {field: 'content', title: '内容'}
            , {field: 'state', title: '状态', width:100, templet: '#switchState', unresize: true}
            , {field: 'sysUserName', title: '发布者'}
            , {field: 'total', title: '可参加人数'}
            , {field: 'totalNow', title: '已参加人数'}
            ,{field: 'isHost', title: '热度', width:85, templet: '#switchIsHost', unresize: true},
            , {fixed: 'right', title:'操作', toolbar: '#bar', width:150}
        ]]
        ,id: 'testReload',
        page: true,
        limits: [20, 50, 100, 200],
        limit: 20
    });

    //监听热度操作
    layui.form.on('switch(isHostCheck)', function(obj){
        var isHost = 1;
        if(obj.elem.checked){
            isHost = 0
        }
        $.ajax({
            url:"/activitys/isHost/",
            type:"post",
            data:{
                id:obj.value,
                isHost:isHost
            },
            success:function (obj) {
                if (obj.code==0) {
                    layer.msg(obj.msg, {
                        icon: 1,
                        shade: 0.3,
                        time: 2000
                    }, function () {
                        //疯狂模式，关闭所有层
                        layui.table.reload();
                    });
                } else {
                    layer.msg(obj.msg, {
                        icon: 2,
                        shade: 0.3,
                        time: 1000
                    });
                }
            }
        })
    });
    //监听状态操作
    layui.form.on('switch(stateCheck)', function(obj){
        var state = 1;
        if(obj.elem.checked){
            state = 0
        }
        $.ajax({
            url:"/activitys/state/",
            type:"post",
            data:{
                id:obj.value,
                state:state
            },
            success:function (obj) {
                if (obj.code==0) {
                    layer.msg(obj.msg, {
                        icon: 1,
                        shade: 0.3,
                        time: 2000
                    }, function () {
                        //疯狂模式，关闭所有层
                        layui.table.reload();
                    });
                } else {
                    layer.msg(obj.msg, {
                        icon: 2,
                        shade: 0.3,
                        time: 1000
                    });
                }
            }
        })
    });
    //头工具栏事件
    layui.table.on('toolbar(tableData)', function(obj){
        var checkStatus = layui.table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'add':
                addForm();
                break;
        };
    });
//监听行工具事件
    layui.table.on('tool(tableData)', function(obj){
        var data = obj.data;
        //删除
        if(obj.event === 'del'){
            layer.confirm('是否删除此记录？此操作不可恢复！', function(index){
                deleteTable(data);
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            editTable(data)
        }
    });

    var $ = layui.$, active = {
        reload: function(){
            var demoReload = $('#demoReload');

            //执行重载
            layui.table.reload('testReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,where: {
                    title: demoReload.val()
                }
            });
        }
    };

    $('.demoTable .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})

/*******新增*********/
function addForm() {
    $("#id").attr("value",'')
    $("#formDiv input").val('')
    layui.layer.open({
        type: 1,
        title: '新增',
        shadeClose: true,//点击遮罩关闭
        anim: 0,
        btnAlign: 'c',
        shade: 0.3,//是否有遮罩，可以设置成false
        maxmin: false, //开启最大化最小化按钮
        area: ['50%', '80%'],
        boolean: true,
        content:$('#formDiv'),
        success: function (layero, lockIndex) {
        },
        end: function () {
            $('#formDiv').hide();
        }
    });
}

/*****删除*******/
function deleteTable(data){
    $.ajax({
        url:"/activitys/delmsg/"+data.id,
        type:"get",
        success:function (obj) {
            if (obj.code==0) {
                layer.msg(obj.msg, {
                    icon: 1,
                    shade: 0.3,
                    time: 2000
                }, function () {
                    reloadTable();  //刷新
                    //疯狂模式，关闭所有层
                    parent.layer.closeAll();
                });
            } else {
                layer.msg(obj.msg, {
                    icon: 2,
                    shade: 0.3,
                    time: 1000
                });
            }
        }
    })

}

/*****刷新表格***********/
function reloadTable() {
    //这里以搜索为例
    layuiTable.reload('testReload', {
        page: {
            curr: 1 //重新从第 1 页开始
        }
    });
}

/*************编辑*************************/
function  editTable(data) {
    layui.layer.open({
        type: 1,
        title: '编辑',
        shadeClose: true,//点击遮罩关闭
        anim: 0,
        btnAlign: 'c',
        shade: 0.3,//是否有遮罩，可以设置成false
        maxmin: false, //开启最大化最小化按钮
        area: ['50%', '80%'],
        boolean: true,
        content:$('#formDiv'),
        success: function (layero, lockIndex) {
            $.ajax({
                url:"/activitys/info/" +data.id,  //根据id查询实体url接口,将记录添加到表单进行编辑修改
                type:"get",
                success:function (r) {
                    if(r.code==0){
                        layui.use(['form'], function() {
                            //表单初始赋值
                            layui.form.val('form', {
                                "id":r.data.id
                                ,"title": r.data.title
                                ,"describe": r.data.describe
                                ,"content": r.data.content
                                ,"state": r.data.state
                                ,"total": r.data.total
                                ,"totalNow": r.data.totalNow
                                ,"isHost": r.data.isHost
                            })
                        });
                    }else {
                        layer.msg(r.msg, {
                            icon: 2,
                            shade: 0.3,
                            time: 1000
                        });
                    }
                }
            })

        },
        end: function () {
            $('#formDiv').hide();
        }
    });
}