var layuiTable;
$(function () {
    // 加载表格数据
    layuiTable =layui.table.render({
        elem: '#tableData'
        , url: '/lost/list/'
        , toolbar: '#toolbarDemo'
        , cols: [[
            {checkbox: true, fixed: true}
            , {field: 'id', title: 'ID', width: 100, unresize: true, sort: true, hide:true}
            , {field: 'title', width:100,title: '标题'}
            , {field: 'pic',  width:100,title: '封面',templet:function(d){
                    return "<img src='"+d.pic+"'>";
            }}
            , {field: 'describe', title: '描述'}
            , {field: 'content', title: '内容'}
            , {field: 'conPic', title:'详细图片'}
            , {field: 'state', minwidth:100,title: '状态',align:'center',templet:function (d) {
                    if(d.state===0){
                        return '过期';
                    }
                    else if(d.state===1){
                        return '正常';
                    }
                    else if(d.state===2){
                        return '已找到';
                    }
                    else if(d.state===3){
                        return '已删除';
                    }
                }}
            , {field: 'audit',minwidth:100, title:'审核状态',align:'center',templet:function(d){
                    if(d.audit===0){
                        return '未审核';
                    }
                    else if(d.audit===1){
                        return '审核通过';
                    }
                    else if(d.audit===2){
                        return '审核未通过';
                    }
                }}
            , {field: 'type', minwidth:100,title: '类型',align:'center',templet:function (d) {
                    if(d.type===0){
                        return '失主发布';
                    }
                    else if(d.type===1)   {
                        return '捡到者发布';
                    }
            }}
            , {field: 'qq', title: 'QQ'}
            , {field: 'userId', title: '用户名'}
            , {fixed: 'right', title:'操作', toolbar: '#bar', width:150}
        ]]
        ,id: 'testReload',
        page: true,
        limits: [20, 50, 100, 200],
        limit: 20
    });


    //头工具栏事件
    layui.table.on('toolbar(tableData)', function(obj){
        var checkStates = layui.table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'add':
                addForm();
                break;
        };
    });
//监听行工具事件
    layui.table.on('tool(tableData)', function(obj){
        var data = obj.data;
        //删除
        if(obj.event === 'del'){
            layer.confirm('是否删除此记录？此操作不可恢复！', function(index){
                deleteTable(data);
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            editTable(data)
        }
    });

    var $ = layui.$, active = {
        reload: function(){
            var demoReload = $('#demoReload');
            //执行重载
                layui.table.reload('testReload', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    ,where: {
                        title: demoReload.val()
                    }
            });
        }
    };

    $('.demoTable .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
})

/*****删除*******/
function deleteTable(data){
    $.ajax({
        url:"/lost/deleteById/"+data.id,
        type:"get",
        success:function (obj) {
            if (obj.code==0) {
                layer.msg(obj.msg, {
                    icon: 1,
                    shade: 0.3,
                    time: 2000
                }, function () {
                    reloadTable();
                    //疯狂模式，关闭所有层
                    parent.layer.closeAll();
                });
            } else {
                layer.msg(obj.msg, {
                    icon: 2,
                    shade: 0.3,
                    time: 1000
                });
            }
        }
    })

}

/********刷新表格******************/
function reloadTable() {
    //这里以搜索为例
    layuiTable.reload('testReload', {
        page: {
            curr: 1 //重新从第 1 页开始
        }
    });
}

/*************编辑*************************/
function  editTable(data) {
    layui.layer.open({
        type: 1,
        title: '编辑',
        shadeClose: true,//点击遮罩关闭
        anim: 0,
        btnAlign: 'c',
        shade: 0.3,//是否有遮罩，可以设置成false
        maxmin: false, //开启最大化最小化按钮
        area: ['50%', '80%'],
        boolean: true,
        content:$('#formDiv'),
        success: function (layero, lockIndex) {
            $.ajax({
                url:"/lost/info/" +data.id,
                type:"get",
                success:function (r) {
                    if(r.code==0){
                        layui.use(['form'], function() {
                            //表单初始赋值
                            layui.form.val('form', {
                                "id": data.id
                                ,"title": r.data.title
                                ,"pic": r.data.pic
                                ,"describe": r.data.describe
                                ,"content": r.data.content
                                ,"state": r.data.state
                                ,"type": r.data.type
                                ,"conPic": r.data.conPic
                                ,"audit": r.data.audit
                                ,"qq": r.data.qq
                                ,"userId": r.data.userId
                            })
                        });
                    }else {
                        layer.msg(r.msg, {
                            icon: 2,
                            shade: 0.3,
                            time: 1000
                        });
                    }

                }
            })

        },
        end: function () {
            $('#formDiv').hide();
        }
    });
}

/*******新增*********/
function addForm() {
    $("#id").attr("value",'')
    $("#formDiv input").val('')
    layui.layer.open({
        type: 1,
        title: '新增',
        shadeClose: true,//点击遮罩关闭
        anim: 0,
        btnAlign: 'c',
        shade: 0.3,//是否有遮罩，可以设置成false
        maxmin: false, //开启最大化最小化按钮
        area: ['50%', '80%'],
        boolean: true,
        content:$('#formDiv'),
        success: function (layero, lockIndex) {
        },
        end: function () {
            $('#formDiv').hide();
        }
    });
}