var upload = layui.upload; //得到 upload 对象
$(function () {
    // 单图上传
    upload.render({
        elem: '#upload_img',
        url: "/oss/upload",
        accept: 'images',
        acceptMime: 'image/*',
        before: function () {
            layer.msg('图片上传中...', {
                icon: 16,
                shade: 0.01,
                time: 0
            });
        },
        done: function (r) {
            console.log(r)
            layer.close(layer.msg());
            if (r.code === 0) {
                alert('上传成功');
            } else {
                alert(r.msg);
            }
        }
    });
})