var protree;
$(function () {
    appendMenuParentHtml();
    $("#menuTree").html('');
    /***************** 加载菜单树****************************/
    layui.config({
        base: '/static/plugins/layui/' //配置 layui 第三方扩展组件存放的基础目录
    }).extend({
        protree: 'js/protree'
    }).use(['protree'], function(){
        protree = layui.protree;
        //后台传入的 标题列表

        loadMenu();
        return false;
    });




})

/***************** 加载菜单树****************************/
function loadMenu() {
    $("#menuTree").html('');
    $.ajax({
        url:"/sys/menu/list",
        type:"GET",
        contentType: "application/json",
        success:function (r) {
            if (r.code == 0) {
                var arr=new Array();
                $.each(r.data,function (index,en) {
                    var t=new Object();
                    t.id=en.menuId;
                    t.name=en.name;
                    t.pid=en.parentId;
                    arr.push(t);
                });
                arr.join(",")
                //执行示例
                protree.init('.menuTree',{
                    arr: arr,
                    close:true,
                    simIcon: "fa fa-file-o",
                    mouIconOpen: "fa fa-folder-open-o",
                    mouIconClose:"fa fa-folder-o",
                    callback: function(id,name) {
                        findMenuByMenuId(id);
                        // alert("你选择的id是" + id + "，名字是" + name);
                    }
                });
            }else{
                layer.msg(r.msg, {
                    icon: 2,
                    shade: 0.3,
                    time: 1000
                });
            }
        }
    })
}

/***********设置下拉选择框内容**************/
function appendMenuParentHtml() {
    $.ajax({
        url:"/sys/menu/list",
        type:"GET",
        contentType: "application/json",
        success:function (r) {
            if (r.code == 0) {
                var str="<option value='0'>一级菜单</option>";
                $.each(r.data,function (index,en) {
                    if(en.parentId==0){
                        str+="<option value='"+en.menuId+"'>"+en.name+"</option>";
                    }
                });
                $("#menuParent").html('<label class="layui-form-label">上级菜单</label><div class="layui-input-block"><select name="parentId" id="parentId" lay-verify="required">'+
                str+'</select></div>');
                layui.form.render();
            }else{
                layer.msg(r.msg, {
                    icon: 2,
                    shade: 0.3,
                    time: 1000
                });
            }
        }
    })

}


/*****************根据id查询*********************/
function findMenuByMenuId(menuId) {
    $.ajax({
        url:"/sys/menu/info/"+menuId,
        type:"GET",
        contentType: "application/json",
        success:function (r) {
            if (r.code == 0) {
                $("#menuId").val(r.data.menuId);
                $("#parentId").val(r.data.parentId);
                $("#name").val(r.data.name)
                $("#url").val(r.data.url)
                $("#icon").val(r.data.icon)
                layui.form.render();
            }else{
                layer.msg(r.msg, {
                    icon: 2,
                    shade: 0.3,
                    time: 1000
                });
            }
        }
    })
}