package com.hebtu.helper.modules.job.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.job.entity.JobEntity;

/**
 * 兼职表
 * @author 小灰灰
 * @date 2019/6/1 10:42
 */


public interface JobDao extends BaseMapper<JobEntity> {

    JobEntity selectByUsername(String jobId);

}
