package com.hebtu.helper.modules.job.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * 用户兼职
 * @author 小灰灰
 * @date 2019/6/1 10:42
 */
@Data
@TableName("user_job")
public class UserJobEntity {
    /**
     * 自增主键id
     */
    @TableId
    private Long id;

    /**
     * 学生学号
     */
    private Long stNo;

    /**
     * 兼职id
     */
    private Long jobId;

    /**
     * 简历地址
     */
    private String resumeUrl;

    /**
     * 状态，（0，删除  1，正常
     */
    private Integer state;

}
