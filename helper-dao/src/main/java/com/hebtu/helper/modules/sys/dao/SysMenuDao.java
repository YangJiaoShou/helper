package com.hebtu.helper.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.sys.entity.SysMenuEntity;

/**
 * 菜单Dao
 * @author YKF
 * @date 2019/5/22 22:34
 */
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {

}
