package com.hebtu.helper.modules.lost.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.lost.entity.LostFindEntity;

/**
 * 失物招领
 * @author LWL
 * @date 2019/5/22 22:54
 */
public interface LostFindDao extends BaseMapper<LostFindEntity> {

}
