package com.hebtu.helper.modules.goods.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.goods.entity.UserGoodsEntity;

/**
 * 用户商品
 * @author 刘肇
 * @date 2019/5/25  23:19
 */
public interface UserGoodsDao extends BaseMapper<UserGoodsEntity> {
}
