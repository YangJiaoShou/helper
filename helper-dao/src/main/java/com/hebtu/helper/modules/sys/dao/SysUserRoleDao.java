package com.hebtu.helper.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.sys.entity.SysUserRoleEntity;

import java.util.List;

/**
 * @author YKF
 * @date 2019/5/22 22:37
 */
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {

    /**
     * 根据用户ID查询他的所有角色
     * @param userId  用户id
     * @return
     */
    List<Long> queryRolesByUserId(Long userId);
}
