package com.hebtu.helper.modules.goods.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.util.Date;
/**
 * 用户商品表
 * @author 刘肇
 * @date 2019/5/25 22:59
 */
@Data
@TableName("user_goods")
public class UserGoodsEntity {

    /**
     * 自增id
     */
    @TableId
    private Long id;
    /**
     * 用户id
     */
    private Long stNo;
    /**
     * 货物编号
     */
    private Long goodId;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;

}
