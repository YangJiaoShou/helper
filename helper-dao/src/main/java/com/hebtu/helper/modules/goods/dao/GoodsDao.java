package com.hebtu.helper.modules.goods.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.goods.entity.GoodsEntity;
/**
 * 商品表
 * @author 刘肇
 * @date 2019/5/25  23:19
 */
public interface GoodsDao extends BaseMapper<GoodsEntity> {

    GoodsEntity selectBySysUserId(String sysUserId);

}

