package com.hebtu.helper.modules.user.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.user.entity.UserEntity;

/**
 * c端用户
 * @author YKF
 * @date 2019/5/22 22:40
 */
public interface UserDao extends BaseMapper<UserEntity> {

    UserEntity selectByStNo(String stNo);
}
