package com.hebtu.helper.modules.job.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 兼职
 * @author 小灰灰
 * @date 2019/6/1 10:42
 */


@Data
@TableName("job")
public class JobEntity implements Serializable {
    /**
     * 自增id
     */
    @TableId
    private Long jobId;

    /**
     * 标题
     */
    private String title;

    /**
     * 描述
     */
    private String describe;

    /**
     * 工资
     */
    private Integer wage;

    /**
     * 地址
     */
    private String address;

    /**
     * 内容
     */
    private String content;

    /**
     * 状态 （0删除，1支持，2过期）
     */
    private Integer state;

    /**
     * 发布者id
     */
    private Long sysUserId;

    /**
     * 发布者昵称
     */
    private String sysUserName;

    /**
     * 可参加人数
     */
    private Long total;

    /**
     * 已参加人数
     */
    private Long totalNow;

    /**
     * 活动发起时间
     */
    private String activitiesTime;

    /**
     * 是否为热点（0：不是，1：是）
     */
    private Integer isHost;

    /**
     * 结束时间
     */
    private String endTime;
}
