package com.hebtu.helper.modules.goods.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品
 * @author 刘肇
 * @date 2019/5/25 22:41
 */
@Data
@TableName("goods")
public class GoodsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId
    private Long goodId;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String describe;
    /**
     * 内容
     */
    private String content;
    /**
     * 价格
     */
    private Double price;
    /**
     * 地址
     */
    private String address;
    /**
     * 状态(0删除 1正常)
     */
    private Integer state;
    /**
     * 发布者ID
     */
    private Long sysUserId;
    /**
     * 发布者昵称
     */
    private String sysUserName;
    /**
     * 热度是否靠前（0不是  1是）
     */
    private Integer isHost;
    /**
     * 发布时间（排序用）（擦亮会更新发布时间）
     */
    private Date releaseDate;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;

}
