package com.hebtu.helper.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.sys.entity.SysRoleEntity;

/**
 * 角色
 * @author YKF
 * @date 2019/5/22 22:35
 */
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {

}
