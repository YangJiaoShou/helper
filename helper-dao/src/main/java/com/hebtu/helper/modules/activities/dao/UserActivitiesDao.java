package com.hebtu.helper.modules.activities.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.activities.entity.UserActivitiesEntity;

/**
 * 用户报名活动
 * @author YKF
 * @date 2019/5/22 22:52
 */
public interface UserActivitiesDao extends BaseMapper<UserActivitiesEntity> {

}
