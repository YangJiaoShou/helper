package com.hebtu.helper.modules.lost.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;
/**
 * 失物招领
 * @author LWL
 * @date 2019/5/22 22:54
 */
@Data
@TableName("lost_find")
public class LostFindEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 封面地址
     */
    private String pic;
    /**
     * 描述
     */
    private String describe;
    /**
     * 内容
     */
    private String content;
    /**
     * 内容图片
     */
    private String conPic;
    /**
     * 状态(0过期	1正常	2已经找到  3已删除)
     */
    private Integer state;
    /**
     * 审核状态（0未审核 1审核通过 2审核未通过）
     */
    private Integer audit;
    /**
     * 类型(0失主发布寻找东西，1捡到者发布寻找失主)
     */
    private Integer type;
    /**
     * QQ
     */
    private String qq;
    /**
     * 发布者id
     */
    private Long userId;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;
}
