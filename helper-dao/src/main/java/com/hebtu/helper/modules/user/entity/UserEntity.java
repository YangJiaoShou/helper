package com.hebtu.helper.modules.user.entity;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * c端用户
 * @author YKF
 * @date 2019/5/22 22:38
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 学号
     */
    private String stNo;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别
     */
    private String  sex;
    /**
     * 头像
     */
    private String headPic;

    /**
     * 学院
     */
    private String jgId;
    /**
     * 专业
     */
    private String bhId;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;

    /**
     * 密码
     */
    @TableField(exist = false)
    private String password;
}
