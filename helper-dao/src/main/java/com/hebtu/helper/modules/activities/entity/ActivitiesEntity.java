package com.hebtu.helper.modules.activities.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;


/**
 * 活动
 * @author YKF
 * @date 2019/5/22 22:48
 */
@Data
@TableName("activities")
public class ActivitiesEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String describe;
    /**
     * 内容
     */
    private String content;
    /**
     * 状态(0删除 1正常 2过期)
     */
    private Integer state;
    /**
     * 发布者
     */
    private Long sysUserId;
    /**
     * 发布者昵称
     */
    private String sysUserName;
    /**
     * 可参加人数
     */
    private Integer total;
    /**
     * 已参加人数
     */
    private Integer totalNow;
    /**
     * 热度是否靠前（0不是  1是）
     */
    private Integer isHost;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;
}
