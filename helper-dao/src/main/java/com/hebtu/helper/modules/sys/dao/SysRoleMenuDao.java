package com.hebtu.helper.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.sys.entity.SysRoleMenuEntity;

import java.util.List;

/**
 * 角色对应菜单
 * @author YKF
 * @date 2019/5/22 22:36
 */
public interface SysRoleMenuDao extends BaseMapper<SysRoleMenuEntity> {

    /**
     * 根据角色查询所有菜单id
     * @param list  角色id
     * @return
     */
    List<Long> queryMenuIdByRoleId(List<Long> list);

}
