package com.hebtu.helper.modules.activities.entity;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *用户报名
 * @author YKF
 * @date 2019/5/22 22:49
 */
@Data
@TableName("user_activities")
public class UserActivitiesEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId
    private Long id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 活动id
     */
    private Long activitiesId;
    /**
     * 手机号
     */
    private String phone;
    /**
     * qq
     */
    private String qq;
    /**
     * 微信
     */
    private String wx;
    /**
     * 0待审核   1已完成   2未守约  3等待 4不符合要求
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 修改时间
     */
    private Date updateDate;
}