package com.hebtu.helper.modules.activities.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.activities.entity.ActivitiesEntity;

/**
 * 活动
 * @author YKF
 * @date 2019/5/22 22:49
 */
public interface ActivitiesDao extends BaseMapper<ActivitiesEntity> {
}
