package com.hebtu.helper.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hebtu.helper.modules.sys.entity.SysUserEntity;

/**
 *  后台用户
 * @author YKF
 * @date 2019/5/22 22:36
 */
public interface SysUserDao extends BaseMapper<SysUserEntity> {

    SysUserEntity selectByUsername(String username);
}